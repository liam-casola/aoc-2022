#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define lbuff 4000

enum OPERATION {
    OPERATION_MULT,
    OPERATION_ADD
};

struct monkey {
    size_t items_count;
    long long int items[50];
    enum OPERATION operation;
    int operation_value;
    int test;
    int true_target;
    int false_target;

    int inspection_count;
};

bool gold_mode = false;
int test_product = 1;

size_t monkeys_count = 0;
struct monkey monkeys[8] = {0};

void take_turn(struct monkey *monkey) {
    for(int i = 0; i < monkey->items_count; ++i) {
        //normalize
        if(gold_mode)
            monkey->items[i] %= test_product;

        //inspect
        if(monkey->operation == OPERATION_MULT) {
            if(monkey->operation_value == -1) {
                monkey->items[i] *= monkey->items[i];
            } else {
                monkey->items[i] *= monkey->operation_value;
            }
        } else {
            if(monkey->operation_value == -1) {
                monkey->items[i] += monkey->items[i];
            } else {
                monkey->items[i] += monkey->operation_value;
            }
        }

        //decrease worry level
        if(!gold_mode)
            monkey->items[i] /= 3;

        //throw
        if(monkey->items[i] % monkey->test) {
            monkeys[monkey->false_target].items[monkeys[monkey->false_target].items_count++] = monkey->items[i];
        } else {
            monkeys[monkey->true_target].items[monkeys[monkey->true_target].items_count++] = monkey->items[i];
        }

        ++monkey->inspection_count;
    }

    monkey->items_count = 0;
}

void do_round() {
    for(int i = 0; i < monkeys_count; ++i) {
        take_turn(&monkeys[i]);
    }
}

long long int sim_rounds(int rounds) {
    for(int i = 0; i < rounds; ++i) {
        do_round();
    }

    long long int monkey_business = 1;
    int active[2] = {0};
    for(int i = 0; i < monkeys_count; ++i) {
        if(monkeys[i].inspection_count > active[1])
            active[1] = monkeys[i].inspection_count;
        for(int j = 1; j > 0; --j)
            if(active[j] > active[j-1]) {
                int tmp = active[j-1];
                active[j-1] = active[j];
                active[j] = tmp;
            }
    }

     for(int i = 0; i < 2; ++i)
        monkey_business *= active[i];
    
    return monkey_business;
}

void print_monkeys() {
    for(int i = 0; i < monkeys_count; ++i) {
        for(int item = 0; item < monkeys[i].items_count; ++item)
            printf("%d ", monkeys[i].items[item]);
        printf("\nop: %d: %d\n", monkeys[i].operation, monkeys[i].operation_value);
        printf("test: %d\n", monkeys[i].test);
        printf("true: %d false: %d\n", monkeys[i].true_target, monkeys[i].false_target);
    }
}

int main()
{
    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        if(buff[0] == '\n')
            fgets(buff, lbuff, stdin); //consume monkey name
        fgets(buff, lbuff, stdin); //get items
        for(int i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
            if(buff[i] - '0' >= 0 && buff[i] - '0' <= 9) {
                monkeys[monkeys_count].items[monkeys[monkeys_count].items_count++] = atoi(&buff[i]);
                for(; buff[i] != ' '  && buff[i] != '\n' && buff[i] != '\0'; ++i) { } //advance past number
            }
        }
        fgets(buff, lbuff, stdin); //get operation
        if(buff[23] == '*')
            monkeys[monkeys_count].operation = OPERATION_MULT;
        else
            monkeys[monkeys_count].operation = OPERATION_ADD;
        if(buff[25] == 'o')
            monkeys[monkeys_count].operation_value = -1;
        else
            monkeys[monkeys_count].operation_value = atoi(&buff[25]);
        fgets(buff, lbuff, stdin); //get test
        monkeys[monkeys_count].test = atoi(&buff[21]);
        fgets(buff, lbuff, stdin); //get true
        monkeys[monkeys_count].true_target = atoi(&buff[29]);
        fgets(buff, lbuff, stdin); //get false
        monkeys[monkeys_count].false_target = atoi(&buff[30]);
        ++monkeys_count;
    }

    struct monkey monkeys_backup[8];
    memcpy(monkeys_backup, monkeys, sizeof(monkeys));

    for(int i = 0; i < monkeys_count; ++i) {
        test_product *= monkeys[i].test;
    }
    
    printf("%lld\n", sim_rounds(20));

    gold_mode = true;
    memcpy(monkeys, monkeys_backup, sizeof(monkeys));

    printf("%lld\n", sim_rounds(10000));

    return 0;
}