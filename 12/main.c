#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
 
#define lbuff 4000

struct square {
    int elevation;
    int steps;
};

int width = 0;
int height = 0;
struct square *squares;
struct square *start;
struct square *end;

struct pos {
    int x;
    int y;
};

size_t poss_count = 0;
struct pos *poss;

struct square * get(int x, int y) {
    return &squares[x + y * width];
}

void set_steps(int x, int y, int steps) {
    squares[x + y * width].steps = steps;
}

void solve(int x, int y) {
    int current_steps = get(x, y)->steps;

    //up
    if(
        y != 0 &&
        get(x, y)->elevation + 1 >= get(x, y - 1)->elevation && //if the square is reachable
        current_steps + 1 < get(x, y - 1)->steps //if there would be a step improvement
    ) {
        set_steps(x, y - 1, current_steps + 1);
        solve(x, y - 1);
    }
    //down
    if(
        y + 1 < height &&
        get(x, y)->elevation + 1 >= get(x, y + 1)->elevation &&
        current_steps + 1 < get(x, y + 1)->steps
    ) {
        set_steps(x, y + 1, current_steps + 1);
        solve(x, y + 1);
    }
    //left
    if(
        x != 0 &&
        get(x, y)->elevation + 1 >= get(x - 1, y)->elevation &&
        current_steps + 1 < get(x - 1, y)->steps
    ) {
        set_steps(x - 1, y, current_steps + 1);
        solve(x - 1, y);
    }
    //right
    if(
        x + 1 < width &&
        get(x, y)->elevation + 1 >= get(x + 1, y)->elevation &&
        current_steps + 1 < get(x + 1, y)->steps
    ) {
        set_steps(x + 1, y, current_steps + 1);
        solve(x + 1, y);
    }
}

void solve_reverse(int x, int y) {
    int current_steps = get(x, y)->steps;

    //up
    if(
        y != 0 &&
        get(x, y - 1)->elevation + 1 >= get(x, y)->elevation && //if the square is reachable
        current_steps + 1 < get(x, y - 1)->steps //if there would be a step improvement
    ) {
        set_steps(x, y - 1, current_steps + 1);
        solve_reverse(x, y - 1);
    }
    //down
    if(
        y + 1 < height &&
        get(x, y + 1)->elevation + 1 >= get(x, y)->elevation &&
        current_steps + 1 < get(x, y + 1)->steps
    ) {
        set_steps(x, y + 1, current_steps + 1);
        solve_reverse(x, y + 1);
    }
    //left
    if(
        x != 0 &&
        get(x - 1, y)->elevation + 1 >= get(x, y)->elevation &&
        current_steps + 1 < get(x - 1, y)->steps
    ) {
        set_steps(x - 1, y, current_steps + 1);
        solve_reverse(x - 1, y);
    }
    //right
    if(
        x + 1 < width &&
        get(x + 1, y)->elevation + 1 >= get(x, y)->elevation &&
        current_steps + 1 < get(x + 1, y)->steps
    ) {
        set_steps(x + 1, y, current_steps + 1);
        solve_reverse(x + 1, y);
    }
}

void reset_squares() {
    for(int y = 0; y < height; ++y)
        for(int x = 0; x < width; ++x)
            set_steps(x, y, INT_MAX);
}

int shortest_total() {
    int shortest = INT_MAX;

    for(int i = 0; i < poss_count; ++i) {
        reset_squares();
        set_steps(poss[i].x, poss[i].y, 0);
        solve(poss[i].x, poss[i].y);
        if(end->steps < shortest)
            shortest = end->steps;
    }

    return shortest;
}

int main()
{
    char buff[lbuff];
    int start_x;
    int start_y;
    int end_x;
    int end_y;
    squares = malloc(sizeof(struct square) * 10000);
    poss = malloc(sizeof(struct pos) * 10000);
    bool tmp = false;
    while (fgets(buff, lbuff, stdin) != NULL) {
        for(int i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
            squares[height * width + i].steps = INT_MAX;
            if(buff[i] == 'S') {
                squares[height * width + i].elevation = 'a' - 'a';
                squares[height * width + i].steps = 0;
                start = &squares[height * width + i];
                start_x = i;
                start_y = height;
            } else if(buff[i] == 'E') {
                squares[height * width + i].elevation = 'z' - 'a';
                end = &squares[height * width + i];
                end_x = i;
                end_y = height;
            } else {
                squares[height * width + i].elevation = buff[i] - 'a';
            }

            if(squares[height * width + i].elevation == 0) {
                poss[poss_count].x = i;
                poss[poss_count].y = height;
                ++poss_count;
            }

            if(!tmp)
                width++;
        }
        tmp = true;
        height++;
    }

    solve(start_x, start_y);

    printf("%d\n", end->steps);

    reset_squares();

    set_steps(end_x, end_y, 0);
    solve_reverse(end_x, end_y);

    int shortest = INT_MAX;

    for(int i = 0; i < poss_count; ++i) {
        if(get(poss[i].x, poss[i].y)->steps < shortest)
            shortest = get(poss[i].x, poss[i].y)->steps;
    }

    printf("%d\n", shortest);

    //printf("%d\n", shortest_total());

    return 0;
}