#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define lbuff 4000

enum opcode {
    NUMBER,
    ADD,
    SUB,
    MUL,
    DIV
};

struct monkey {
    enum opcode op;
    int lhs;
    int rhs;
    bool human_tainted;
    long long int res;
};

//monkeys
#define root 0x9B9D1
#define humn 0x6B287
#define monkey_combo (31*31*31*31)
struct monkey *monkeys;

long long int monkey_calculator(struct monkey *monkey) {
    if(monkey->res == LONG_LONG_MAX) {
        switch (monkey->op) {
            case NUMBER:
                break;
            case ADD:
                monkey->res = monkey_calculator(&monkeys[monkey->lhs]) + monkey_calculator(&monkeys[monkey->rhs]);
                monkey->human_tainted |= monkeys[monkey->lhs].human_tainted;
                monkey->human_tainted |= monkeys[monkey->rhs].human_tainted;
                break;
            case SUB:
                monkey->res = monkey_calculator(&monkeys[monkey->lhs]) - monkey_calculator(&monkeys[monkey->rhs]);
                monkey->human_tainted |= monkeys[monkey->lhs].human_tainted;
                monkey->human_tainted |= monkeys[monkey->rhs].human_tainted;
                break;
            case MUL:
                monkey->res = monkey_calculator(&monkeys[monkey->lhs]) * monkey_calculator(&monkeys[monkey->rhs]);
                monkey->human_tainted |= monkeys[monkey->lhs].human_tainted;
                monkey->human_tainted |= monkeys[monkey->rhs].human_tainted;
                break;
            case DIV:
                monkey->res = monkey_calculator(&monkeys[monkey->lhs]) / monkey_calculator(&monkeys[monkey->rhs]);
                monkey->human_tainted |= monkeys[monkey->lhs].human_tainted;
                monkey->human_tainted |= monkeys[monkey->rhs].human_tainted;
                break;
        }
    }

    return monkey->res;
}

void human_math(struct monkey *monkey) {
    switch (monkey->op) {
        case NUMBER:
            break;
        case ADD:
            if(monkeys[monkey->lhs].human_tainted) {
                monkeys[monkey->lhs].res = monkey->res - monkeys[monkey->rhs].res;
                human_math(&monkeys[monkey->lhs]);
            }
            if(monkeys[monkey->rhs].human_tainted) {
                monkeys[monkey->rhs].res = monkey->res - monkeys[monkey->lhs].res;
                human_math(&monkeys[monkey->rhs]);
            }
            break;
        case SUB:
            if(monkeys[monkey->lhs].human_tainted) {
                monkeys[monkey->lhs].res = monkey->res + monkeys[monkey->rhs].res;
                human_math(&monkeys[monkey->lhs]);
            }
            if(monkeys[monkey->rhs].human_tainted) {
                monkeys[monkey->rhs].res = -monkey->res + monkeys[monkey->lhs].res;
                human_math(&monkeys[monkey->rhs]);
            }
            break;
        case MUL:
            if(monkeys[monkey->lhs].human_tainted) {
                monkeys[monkey->lhs].res = monkey->res / monkeys[monkey->rhs].res;
                human_math(&monkeys[monkey->lhs]);
            }
            if(monkeys[monkey->rhs].human_tainted) {
                monkeys[monkey->rhs].res = monkey->res / monkeys[monkey->lhs].res;
                human_math(&monkeys[monkey->rhs]);
            }
            break;
        case DIV:
            if(monkeys[monkey->lhs].human_tainted) {
                monkeys[monkey->lhs].res = monkey->res * monkeys[monkey->rhs].res;
                human_math(&monkeys[monkey->lhs]);
            }
            if(monkeys[monkey->rhs].human_tainted) {
                monkeys[monkey->rhs].res = monkeys[monkey->lhs].res / monkey->res;
                human_math(&monkeys[monkey->rhs]);
            }
            break;
    }
}

long long int human_equality(struct monkey *monkey) {
    if(monkeys[monkey->lhs].human_tainted) {
        monkeys[monkey->lhs].res = monkeys[monkey->rhs].res;
        human_math(&monkeys[monkey->lhs]);
    }
    if(monkeys[monkey->rhs].human_tainted) {
        monkeys[monkey->rhs].res = monkeys[monkey->lhs].res;
        human_math(&monkeys[monkey->rhs]);
    }

    return monkeys[humn].res;
}

int main() {
    monkeys = malloc(sizeof(struct monkey) * monkey_combo);

    char *buff = malloc(lbuff);
    while (fgets(buff, lbuff, stdin) != NULL) {
        unsigned monkey_index = 0;
        monkey_index |= buff[0] - 'a';
        monkey_index |= (buff[1] - 'a') << 5;
        monkey_index |= (buff[2] - 'a') << 10;
        monkey_index |= (buff[3] - 'a') << 15;

        monkeys[monkey_index].human_tainted = false;

        if(buff[6] >= '0' && buff[6] <= '9') {
            monkeys[monkey_index].op = NUMBER;
            monkeys[monkey_index].res = atoi(&buff[6]);
            continue;
        }

        //INT_MAX = NULL result
        monkeys[monkey_index].res = LONG_LONG_MAX;

        unsigned lhs_index = 0;
        lhs_index |= buff[6] - 'a';
        lhs_index |= (buff[7] - 'a') << 5;
        lhs_index |= (buff[8] - 'a') << 10;
        lhs_index |= (buff[9] - 'a') << 15;
        monkeys[monkey_index].lhs = lhs_index;

        switch (buff[11]) {
            case '+': monkeys[monkey_index].op = ADD; break;
            case '-': monkeys[monkey_index].op = SUB; break;
            case '*': monkeys[monkey_index].op = MUL; break;
            case '/': monkeys[monkey_index].op = DIV; break;
        }

        unsigned rhs_index = 0;
        rhs_index |= buff[13] - 'a';
        rhs_index |= (buff[14] - 'a') << 5;
        rhs_index |= (buff[15] - 'a') << 10;
        rhs_index |= (buff[16] - 'a') << 15;
        monkeys[monkey_index].rhs = rhs_index;
    }
    free(buff);

    monkeys[humn].human_tainted = true;
    printf("%lld\n", monkey_calculator(&monkeys[root]));

    printf("%lld\n", human_equality(&monkeys[root]));

    free(monkeys);

    return 0;
}