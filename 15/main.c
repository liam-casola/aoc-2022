#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define lbuff 4000

struct pos {
    int x;
    int y;
};

struct sensor {
    struct pos sensor;
    int beacon_idx;
};

struct range {
    int start;
    int end;
};

size_t beacons_count = 0;
struct pos *beacons = NULL;

int find_beacon(struct pos *beacons, size_t count, int x, int y) {
    int idx = -1;
    for(int i = 0; i < count; ++i)
        if(beacons[i].x == x && beacons[i].y == y) {
            idx = i;
            break;
        }
    return idx;
}

int beacon_distance(struct sensor *sensor) {
    int delta_x = abs(sensor->sensor.x - beacons[sensor->beacon_idx].x);
    int delta_y = abs(sensor->sensor.y - beacons[sensor->beacon_idx].y);
    return delta_x + delta_y;
}

void sensor_row_coverage(struct sensor *sensor, struct range *range, int y) {
    int d = beacon_distance(sensor);
    range->start = sensor->sensor.x - (d - abs(sensor->sensor.y - y));
    range->end = sensor->sensor.x + (d - abs(sensor->sensor.y - y));
}

void sensor_col_coverage(struct sensor *sensor, struct range *range, int x) {
    int d = beacon_distance(sensor);
    range->start = sensor->sensor.y - (d - abs(sensor->sensor.x - x));
    range->end = sensor->sensor.y + (d - abs(sensor->sensor.x - x));
}

void sort_ranges(struct range *ranges, size_t count) {
    for(int i = 0; i < count; ++i) {
        for(int j = 0; j < count - 1; ++j) {
            if(
                ranges[j].start > ranges[j+1].start ||
                (ranges[j].start == ranges[j+1].start &&
                ranges[j].end > ranges[j+1].end)
            ) {
                struct range tmp = ranges[j];
                ranges[j] = ranges[j+1];
                ranges[j+1] = tmp;
            }
        }
    }
}

int merge_ranges(struct range *ranges, size_t count) {
    if(ranges[0].end < ranges[0].start) {
        //remove the ranges not on the current row
        for(int i = 0; i < count - 1; ++i) {
            ranges[i] = ranges[i+1];
        }
        --count;
    }

    for(int i = 0; i < count - 1; ++i) {
        if(ranges[i+1].end < ranges[i+1].start) {
            //remove the ranges not on the current row
            for(int j = i + 1; j < count - 1; ++j) {
                ranges[j] = ranges[j+1];
            }
            --count;
            --i;
        } else

        if(ranges[i].end >= ranges[i+1].start) {
            if(ranges[i].end < ranges[i+1].end)
                ranges[i].end = ranges[i+1].end;
            //shift elements left 1
            for(int j = i + 1; j < count - 1; ++j) {
                ranges[j] = ranges[j+1];
            }
            --count;
            --i;
        }
    }

    return count;
}

//part1
int build_ranges(struct sensor *sensors, size_t count, int row) {
    struct range *ranges = malloc(sizeof(struct range) * count);

    for(int i = 0; i < count; ++i) {
        sensor_row_coverage(&sensors[i], &ranges[i], row);
    }

    sort_ranges(ranges, count);

    int range_count = merge_ranges(ranges, count);

    int positions = 0;
    for(int i = 0; i < range_count; ++i) {
        positions += ranges[i].end - ranges[i].start;
    }

    free(ranges);

    return positions;
}

//part 2
long long int build_ranges_gold(struct sensor *sensors, size_t count) {
    struct range *ranges = malloc(sizeof(struct range) * count);

    long long int row = 0;
    for(; row <= 4000000; ++row) {
        for(int i = 0; i < count; ++i) {
            sensor_row_coverage(&sensors[i], &ranges[i], row);
        }

        sort_ranges(ranges, count);

        int range_count = merge_ranges(ranges, count);

        int positions = 0;
        for(int i = 0; i < range_count; ++i) {
            if(ranges[i].start < 0) ranges[i].start = 0;
            if(ranges[i].end > 4000000) ranges[i].end = 4000000;
            positions += ranges[i].end - ranges[i].start;
        }

        if(positions < 4000000)
            break;
    }

    long long int col = 0;
    for(; col <= 4000000; ++col) {
        for(int i = 0; i < count; ++i) {
            sensor_col_coverage(&sensors[i], &ranges[i], col);
        }

        sort_ranges(ranges, count);

        int range_count = merge_ranges(ranges, count);

        int positions = 0;
        for(int i = 0; i < range_count; ++i) {
            if(ranges[i].start < 0) ranges[i].start = 0;
            if(ranges[i].end > 4000000) ranges[i].end = 4000000;
            positions += ranges[i].end - ranges[i].start;
        }

        if(positions < 4000000)
            break;
    }

    free(ranges);

    return row + col * 4000000;
}

int main()
{
    size_t sensors_count = 0;
    struct sensor *sensors = NULL;

    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        sensors = realloc(sensors, sizeof(struct sensor) * ++sensors_count);

        int i = 0;
        for(; buff[i] != '='; ++i) { }
        sensors[sensors_count - 1].sensor.x = atoi(&buff[++i]);
        for(; buff[i] != '='; ++i) { }
        sensors[sensors_count - 1].sensor.y = atoi(&buff[++i]);
        for(; buff[i] != '='; ++i) { }
        int beacon_x = atoi(&buff[++i]);
        for(; buff[i] != '='; ++i) { }
        int beacon_y = atoi(&buff[++i]);
        
        int beacon_idx = find_beacon(beacons, beacons_count, beacon_x, beacon_y);
        if(beacon_idx != -1) {
            sensors[sensors_count - 1].beacon_idx = beacon_idx;
        } else {
            beacons = realloc(beacons, sizeof(struct pos) * ++beacons_count);
            beacons[beacons_count - 1].x = beacon_x;
            beacons[beacons_count - 1].y = beacon_y;
            sensors[sensors_count - 1].beacon_idx = beacons_count - 1;
        }
    }

    int silver = build_ranges(sensors, sensors_count, 2000000);
    printf("%d\n", silver);

    long long int gold = build_ranges_gold(sensors, sensors_count);
    printf("%lld\n", gold);

    return 0;
}