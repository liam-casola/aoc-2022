#include <stdio.h>
#include <stdlib.h>
 
#define lbuff 4000

enum opcode {
    NOOP = 1,
    ADDX = 2,
};

struct CPU {
    int cycle;
    int rX;
};

#define width 40
#define height 6
struct CRT {
    char pixel[width * height];
} crt0;

struct instruction {
    enum opcode op;
    int data;
};

size_t instructions_count = 0;
struct instruction instructions[200];

int strength_sum = 0;

void advance_cycle(struct CPU *cpu) {
    if(cpu->cycle - 20 == 0) {
        strength_sum += cpu->cycle * cpu->rX;
    } else if(!((cpu->cycle - 20) % 40)) {
        strength_sum += cpu->cycle * cpu->rX;
    }

    if((cpu->cycle - 1) % 40 == cpu->rX
    || (cpu->cycle - 1) % 40 == cpu->rX - 1
    || (cpu->cycle - 1) % 40 == cpu->rX + 1)
        crt0.pixel[cpu->cycle - 1] = '#';
    else
        crt0.pixel[cpu->cycle - 1] = '.';

    ++cpu->cycle;
}

void process_op(struct CPU *cpu, struct instruction *inst) {
    switch (inst->op)
    {
    case ADDX:
        for(int i = 0; i < 2; ++i)
            advance_cycle(cpu);
        cpu->rX += inst->data;
        break;
    default:
        advance_cycle(cpu);
        break;
    }
}

int signal_strength() {
    strength_sum = 0;

    struct CPU cpu0 = {1, 1};
    for(int i = 0; i < instructions_count; ++i) {
        process_op(&cpu0, &instructions[i]);
    }

    return strength_sum;
}

void print_crt() {
    for(int y = 0; y < height; ++y) {
        for(int x = 0; x < width; ++x)
            printf("%c", crt0.pixel[x + y * width]);
        printf("\n");
    }
}

int main()
{
    char buff[lbuff];
    int tmp = 0;
    while (fgets(buff, lbuff, stdin) != NULL) {
        switch (buff[0]) {
        case 'a':
            instructions[instructions_count].op = ADDX;
            instructions[instructions_count].data = atoi(&buff[5]);
            break;
        case 'n':
            instructions[instructions_count].op = NOOP;
            break;
        }
        ++instructions_count;
    }

    printf("%d\n", signal_strength());

    print_crt();

    return 0;
}