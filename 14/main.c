#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define lbuff 4000

struct map {
    int width;
    int height;
    int smallest_x;
    char *rocks;
};

char get(struct map *map, int x, int y) {
    return map->rocks[y * map->width + x - map->smallest_x];
}

void set(struct map *map, int x, int y, char c) {
    map->rocks[y * map->width + x - map->smallest_x] = c;
}

struct map * create_map(int width, int height, int smallest_x) {
    struct map *map = malloc(sizeof(struct map));
    map->width = width;
    map->height = height;
    map->smallest_x = smallest_x;
    map->rocks = malloc(width * height);
    memset(map->rocks, '.', width * height);

    return map;
}

void delete_map(struct map *map) {
    free(map->rocks);
    free(map);
}

struct map * update_map(struct map *map, int width, int height, int smallest_x) {
    struct map *dst = create_map(width, height, smallest_x);

    for(int y = 0; y < map->height; ++y) {
        for(int x = map->smallest_x; x < map->smallest_x + map->width; ++x) {
            set(dst, x, y, get(map, x, y));
        }
    }

    delete_map(map);

    return dst;
}

void print_map(struct map *map) {
    for(int y = 0; y < map->height; ++y) {
        for(int x = map->smallest_x; x < map->smallest_x + map->width; ++x) {
            printf("%c", get(map, x, y));
        }
        printf("\n");
    }
}

//------------part1------------
bool drop_sand(struct map *map) {
    int x = 500;
    int y = 0;
    bool overflow = false;
    while(1) {
        if(y != map->height - 1) { //check not about to fall into the void
            if(get(map, x, y + 1) == '.') { //check room below
                ++y;
            } else if(x == map->smallest_x) { //check not about to fall into void left
                overflow = true;
                break;
            } else if (get(map, x - 1, y + 1) == '.') { //check room below and left
                ++y;
                --x;
            } else if(x == map->smallest_x + map->width - 1) { //check not about to fall into void right
                overflow = true;
                break;
            } else if(get(map, x + 1, y + 1) == '.') { //check room below and right
                ++y;
                ++x;
            } else { //sand cannot move any further
                set(map, x, y, 'o');
                break;
            }
        } else {
            overflow = true;
            break;
        }
    }

    if(x == 500 && y == 0)
        overflow = true;

    return overflow;
}

int sand_overflow(struct map *map) {
    int sand = 0;
    while(!drop_sand(map)) {
        ++sand;
    }
    return sand;
}

int main()
{
    struct map *map = create_map(1, 1, 500);
    set(map, 500, 0, '+');

    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        int prev_x;
        int prev_y;
        bool prev = false;
        for(int i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
            if(buff[i] >= '0' && buff[i] <= '9') {
                int x = atoi(&buff[i]);
                while(buff[i] >= '0' && buff[i] <= '9') { ++i; } // consume x numbers
                ++i; // consume ','
                int y = atoi(&buff[i]);
                while(buff[i] >= '0' && buff[i] <= '9') { ++i; } // consume y numbers
                
                //compute any new map sizes
                int width = map->width;
                int height = map->height;
                int smallest_x = map->smallest_x;
                if(x < map->smallest_x) {
                    width += smallest_x - x;
                    smallest_x = x;
                }

                if(x >= map->smallest_x + map->width) {
                    width = x - smallest_x + 1;
                }

                if(y >= map->height) {
                    height = y + 1;
                }

                //update the map if there are new sizes
                if(width != map->width || height != map->height || smallest_x != map->smallest_x) {
                    map = update_map(map, width, height, smallest_x);
                }

                //fill in the new rock line
                if(prev) {
                    int low_x = prev_x <= x ? prev_x : x;
                    int high_x = prev_x > x ? prev_x : x;
                    for(; low_x <= high_x; ++low_x)
                        set(map, low_x, y, '#');
                    int low_y = prev_y <= y ? prev_y : y;
                    int high_y = prev_y > y ? prev_y : y;
                    for(; low_y <= high_y; ++low_y)
                        set(map, x, low_y, '#');
                }

                prev_x = x;
                prev_y = y;
                prev = true;
            }
        }
    }

    struct map *gold_map = create_map(map->width, map->height, map->smallest_x);
    memcpy(gold_map->rocks, map->rocks, map->width * map->height);

    printf("%d\n", sand_overflow(map));
    //print_map(map);

    gold_map = update_map(gold_map, 4000, gold_map->height + 2, gold_map->smallest_x - 2000);
    for(int x = gold_map->smallest_x; x < gold_map->smallest_x + gold_map->width; ++x)
        set(gold_map, x, gold_map->height - 1, '#');
    //print_map(gold_map);
    printf("%d\n", sand_overflow(gold_map) + 1);

    return 0;
}