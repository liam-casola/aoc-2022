#include <stdio.h>
#include <stdlib.h>
 
#define lbuff 4000
#define ldata lbuff

unsigned char data[ldata];

struct assignment {
    int start;
    int end;
};

struct pair {
    struct assignment e1;
    struct assignment e2;
};

size_t pairs_count = 0;
struct pair pairs[2000];

int full_overlap() {
    int sum = 0;

    for(int i = 0; i < pairs_count; ++i) {
        if(
            (pairs[i].e1.start <= pairs[i].e2.start) && (pairs[i].e1.end >= pairs[i].e2.end) ||
            (pairs[i].e2.start <= pairs[i].e1.start) && (pairs[i].e2.end >= pairs[i].e1.end)
        ) {
            ++sum;
        }
    }

    return sum;
}

int partial_overlap() {
    int sum = 0;

    for(int i = 0; i < pairs_count; ++i) {
        if(
            (pairs[i].e1.start <= pairs[i].e2.end) && (pairs[i].e1.start >= pairs[i].e2.start) ||
            (pairs[i].e1.end >= pairs[i].e2.start) && (pairs[i].e1.end <= pairs[i].e2.end) ||
            (pairs[i].e2.start <= pairs[i].e1.end) && (pairs[i].e2.start >= pairs[i].e1.start) ||
            (pairs[i].e2.end >= pairs[i].e1.start) && (pairs[i].e2.end <= pairs[i].e1.end)
        ) {
            ++sum;
        }
    }

    return sum;
}

int main()
{
    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        int i = 0;
        pairs[pairs_count].e1.start = atoi(buff);
        for(; buff[i] != '-'; ++i) { }
        pairs[pairs_count].e1.end = atoi(&buff[i+1]);
        for(; buff[i] != ','; ++i) { }
        pairs[pairs_count].e2.start = atoi(&buff[i+1]);
        for(; buff[i] != '-'; ++i) { }
        pairs[pairs_count].e2.end = atoi(&buff[i+1]);
        ++pairs_count;
    }

    printf("%d\n", full_overlap());

    printf("%d\n", partial_overlap());

    return 0;
}