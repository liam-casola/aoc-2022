#include <stdio.h>
#include <stdlib.h>
 
#define lbuff 4000
#define ldata lbuff

unsigned char data[ldata];

struct rucksack {
    size_t length;
    char contents[100];
    int c1[52];
    int c2[52];
};

size_t rucksack_count = 0;
struct rucksack rucksacks[1000] = {0};

void process_compartments() {
    for(int i = 0; i < rucksack_count; ++i) {
        for(int j = 0; j < rucksacks[i].length / 2; ++j) {
            char item = rucksacks[i].contents[j];
            if(item >= 'a' && item <= 'z')
                ++rucksacks[i].c1[item - 'a'];
            if(item >= 'A' && item <= 'Z')
                ++rucksacks[i].c1[item - 'A' + 26];
        }

        for(int j = rucksacks[i].length / 2; j < rucksacks[i].length; ++j) {
            char item = rucksacks[i].contents[j];
            if(item >= 'a' && item <= 'z')
                ++rucksacks[i].c2[item - 'a'];
            if(item >= 'A' && item <= 'Z')
                ++rucksacks[i].c2[item - 'A' + 26];
        }   
    }
}

int sum_duplicates() {
    int total = 0;
    for(int i = 0; i < rucksack_count; ++i) {
        for(int j = 0; j < 52; ++j) {
            if(rucksacks[i].c1[j] > 0 && rucksacks[i].c2[j] > 0)
                total += j + 1;
        }
    }
    return total;
}

int sum_badges() {
    int total = 0;
    for(int i = 0; i < rucksack_count; i+=3) {
        for(int j = 0; j < 52; ++j) {
            if(
                (rucksacks[i].c1[j] > 0 || rucksacks[i].c2[j] > 0) &&
                (rucksacks[i+1].c1[j] > 0 || rucksacks[i+1].c2[j] > 0) &&
                (rucksacks[i+2].c1[j] > 0 || rucksacks[i+2].c2[j] > 0)
            ) {
                total += j + 1;
            }
        }
    }
    return total;
}

int main()
{
    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        int i;
        for(i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
            rucksacks[rucksack_count].contents[i] = buff[i];
        }
        rucksacks[rucksack_count].length = i;
        ++rucksack_count;
    }

    process_compartments();

    printf("%d\n", sum_duplicates());

    printf("%d\n", sum_badges());

    return 0;
}