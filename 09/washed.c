#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
 
#define lbuff 100

struct motion {
    char dir;
    int steps;
};

size_t motions_count = 0;
size_t motions_size = 8;
struct motion *motions;

struct pos {
    int x;
    int y;
};

struct pos rope[10] = {0};

int max_x = 0;
int min_x = 0;
int max_y = 0;
int min_y = 0;
int visited_count = 0;
size_t width = 1;
size_t height = 1;
bool *visited;

bool * visited_at(int x, int y) {
    return (visited + width * (y - min_y) + (x - min_x));
}

void add_visited(int x, int y) {
    bool *has_visited = visited_at(x,y);
    if(!*has_visited) {
        *has_visited = true;
        ++visited_count;
    }
}

void move_tail(struct pos *tail, struct pos *head, bool flag) {
    int hor_d = head->x - tail->x;
    int ver_d = head->y - tail->y;

    bool diag = abs(hor_d) + abs(ver_d) > 2;

    int hor = 0;
    int ver = 0;

    if(hor_d > 1 || (hor_d > 0 && diag)) hor = 1;
    if(hor_d < -1 || (hor_d < 0 && diag)) hor = -1;
    if(ver_d > 1 || (ver_d > 0 && diag)) ver = 1;
    if(ver_d < -1 || (ver_d < 0 && diag)) ver = -1;

    // branchless version, ~20% slower
    // int hor_sign = (hor_d > 0) - (hor_d < 0);
    // int ver_sign = (ver_d > 0) - (ver_d < 0);

    // int hor = hor_d - hor_sign | diag * hor_sign;
    // int ver = ver_d - ver_sign | diag * ver_sign;

    tail->x += hor;
    tail->y += ver;

    if(flag)
        add_visited(tail->x, tail->y);
}

void move_head(struct pos *head, struct motion *motion, int knots) {
    int hor = 0;
    int ver = 0;

    switch (motion->dir) {
    case 'U':
        ver = 1;
        break;
    case 'D':
        ver = -1;
        break;
    case 'R':
        hor = 1;
        break;
    case 'L':
        hor = -1;
        break;
    }

    for(int i = 0; i < motion->steps; ++i) {
        head->x += hor;
        head->y += ver;
        for(int j = 1; j < knots; ++j)
            move_tail(&rope[j], &rope[j-1], j == knots - 1);
    }
}

int simulate(int knots) {
    //rest rope
    for(int i = 0; i < 10; ++i) {
        rope[i].x = 0;
        rope[i].y = 0;
    }

    //reset tail visits
    visited_count = 0;
    for(long long int i = 0; i < width * height; ++i)
        visited[i] = false;

    for(int i = 0; i < motions_count; ++i)
        move_head(&rope[0], &motions[i], knots);

    return visited_count;
}

void parse_input() {
    char buff[lbuff];
    motions = (struct motion *)malloc(sizeof(struct motion) * motions_size);

    int x = 0;
    int y = 0;
    while (fgets(buff, lbuff, stdin) != NULL) {
        motions[motions_count].dir = buff[0];
        motions[motions_count].steps = atoi(&buff[2]);

        switch (motions[motions_count].dir) {
        case 'U':
            y += motions[motions_count].steps;
            if(y > max_y) max_y = y;
            break;
        case 'D':
            y -= motions[motions_count].steps;
            if(y < min_y) min_y = y;
            break;
        case 'R':
            x += motions[motions_count].steps;
            if(x > max_x) max_x = x;
            break;
        case 'L':
            x -= motions[motions_count].steps;
            if(x < min_x) min_x = x;
            break;
        }

        if(motions_count + 1 == motions_size) {
            motions_size *= 2;
            motions = (struct motion *)realloc(motions, sizeof(struct motion) * motions_size);
        }
        motions_count++;
    }

    width += max_x - min_x;
    height += max_y - min_y;
    visited = (bool *)malloc(sizeof(bool) * width * height);
}

int main()
{
    clock_t start_parse = clock();
    parse_input();
    clock_t elapsed_parse = clock() - start_parse;

    clock_t start_silver = clock();
    printf("%d\n", simulate(2));
    clock_t elapsed_silver = clock() - start_silver;

    clock_t start_gold = clock();
    printf("%d\n", simulate(10));
    clock_t elapsed_gold = clock() - start_gold;

    free(motions);
    free(visited);

    printf("\nparse:  %.3fs\n", (double)elapsed_parse / CLOCKS_PER_SEC);
    printf("silver: %.3fs\n", (double)elapsed_silver/ CLOCKS_PER_SEC);
    printf("gold:   %.3fs\n", (double)elapsed_gold / CLOCKS_PER_SEC);

    return 0;
}