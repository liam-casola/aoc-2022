#include <stdio.h>
#include <stdlib.h>
 
#define lbuff 4000
#define ldata lbuff

unsigned char data[ldata];

struct round {
    int abc;
    int xyz;
    int outcome;
    int score;
    int score2;
};

size_t round_count = 0;
struct round rounds[10000];

void score_round(struct round *round) {
    if(round->abc == round->xyz)
        round->score = round->xyz + 3;
    else if(round->abc % 3 == round->xyz - 1)
        round->score = round->xyz + 6;
    else
        round->score = round->xyz;

    if(round->outcome == 3)
        round->score2 = round->abc + 3;
    else if(round->outcome == 6)
        round->score2 = round->abc % 3 + 1 + 6;
    else
        round->score2 = (round->abc + 1) % 3 + 1;
}

void total_score(int *score, int *score2) {
    for(int i = 0; i < round_count; ++i) {
        *score += rounds[i].score;
        *score2 += rounds[i].score2;
    }
}

int main()
{
    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        rounds[round_count].abc = (buff[0] - 'A') + 1;
        rounds[round_count].xyz = (buff[2] - 'X') + 1;
        rounds[round_count].outcome = (buff[2] - 'X') * 3;
        score_round(&rounds[round_count]);
        ++round_count;
    }

    int p1 = 0, p2 = 0;
    total_score(&p1, &p2);

    printf("%d\n", p1);

    printf("%d\n", p2);

    return 0;
}