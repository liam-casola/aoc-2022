#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define lbuff 4000

int time_limit = 30;

struct valve;

struct valve {
    char label[2];
    int flow_rate;
    bool open;
    size_t moves_count;
    struct valve *moves[10];
    //distance to each valve at index matching valves
    int distance;
    int distance_valves[100];
};

//time, current valve, 15 valve states
//00000'0000'000000000000000
int *pressure_map;

size_t valves_count = 0;
struct valve valves[100] = {0};

size_t pressure_valves_count = 0;
struct valve pressure_valves[16] = {0};

//maps the pressure valve at index to it's index in the valves array
int pv_to_v_map[16];

int find_valve(char label[2]) {
    for(int i = 0; i < valves_count; ++i)
        if(memcmp(label, valves[i].label, 2) == 0)
            return i;
    return valves_count++;
}

void print_valves() {
    for(int i = 0; i < valves_count; ++i) {
        printf("%c%c %d ->", valves[i].label[0], valves[i].label[1], valves[i].flow_rate);
        for(int j = 0; j < valves[i].moves_count; ++j) {
            printf(" %c%c,", valves[i].moves[j]->label[0], valves[i].moves[j]->label[1]);
        }
        printf("\n");
    }
}

void print_valve_distances() {
    for(int i = 0; i < valves_count; ++i) {
        printf("%c%c %d ->", valves[i].label[0], valves[i].label[1], valves[i].flow_rate);
        for(int j = 0; j < valves_count; ++j) {
            printf(" %d:", valves[i].distance_valves[j]);
            printf("%c%c,", valves[j].label[0], valves[j].label[1]);
        }
        printf("\n");
    }
}

void shortest_path_to_valve(struct valve *start) {
    for(int i = 0; i < start->moves_count; ++i) {
        if(start->distance + 1 < start->moves[i]->distance) {
            start->moves[i]->distance = start->distance + 1;
            shortest_path_to_valve(start->moves[i]);
        }
    }
}

//----------part1------------
int best_pressure(struct valve *valve, int minute) {
    if(minute >= time_limit)
        return 0;

    int largest_pressure = 0;
    valve->open = true;
    //build index to map
    unsigned idx = 0;
    //valve states
    for(int i = 0; i < pressure_valves_count; ++i) {
        idx |= pressure_valves[i].open << i;
    }
    //current valve idx
    int cur_v_idx = find_valve(valve->label);
    int cur_pv_idx = 0;
    for(int i = 0; i < pressure_valves_count; ++i)
        if(pv_to_v_map[i] == cur_v_idx)
            cur_pv_idx = i;
    idx |= cur_pv_idx << 15;
    //minute
    idx |= minute << 19;
    //printf("%d %d %08X\n", cur_pv_idx, minute, idx);
    //lookup pressure, if none, calculate, otherwise return pressure
    if(pressure_map[idx] < 0) {
        for(int i = 0; i < pressure_valves_count; ++i) {
            if(pressure_valves[i].open == false) {
                int pressure = best_pressure(&pressure_valves[i], minute + valve->distance_valves[pv_to_v_map[i]] + 1);
                if(pressure > largest_pressure)
                    largest_pressure = pressure;
            }
        }

        largest_pressure += valve->flow_rate * (time_limit - minute);
        pressure_map[idx] = largest_pressure;
    } else {
        largest_pressure = pressure_map[idx];
    }
    valve->open = false;

    return largest_pressure;
}

//----------part2------------
int best_pressure_2(struct valve *valve_me, struct valve *valve_ele, int min_me, int min_ele, bool me) {
    if(min_me >= time_limit && min_ele >= time_limit)
        return 0;

    if(min_me > time_limit)
        min_me = time_limit;

    if(min_ele > time_limit)
        min_ele = time_limit;

    int largest_pressure = 0;
    //build index to map
    unsigned idx = 0;
    //valve states
    for(int i = 0; i < pressure_valves_count; ++i) {
        idx |= pressure_valves[i].open << i;
    }
    //current valve idx
    int cur_v_idx;
    if(me)
        cur_v_idx = find_valve(valve_me->label);
    else
        cur_v_idx = find_valve(valve_ele->label);

    int cur_pv_idx = 0;
    for(int i = 0; i < pressure_valves_count; ++i)
        if(pv_to_v_map[i] == cur_v_idx)
            cur_pv_idx = i;
    idx |= me << 15;
    idx |= cur_pv_idx << 16;
    //minute
    if(me) {
        idx |= min_me << 20;
    } else {
        idx |= min_ele << 20;
    }
    int minute = me ? min_me : min_ele;
    //lookup pressure, if none, calculate, otherwise return pressure
    if(pressure_map[idx] < 0) {
        int hit = false;
        for(int i = 0; i < pressure_valves_count; ++i) {
            if(pressure_valves[i].open == false) {
                hit = true;
                pressure_valves[i].open = true;
                int pressure;
                if(min_me <= min_ele) {
                    pressure = best_pressure_2(&pressure_valves[i], valve_ele, min_me + valve_me->distance_valves[pv_to_v_map[i]] + 1, min_ele, true);
                } else {
                    pressure = best_pressure_2(valve_me, &pressure_valves[i], min_me, min_ele + valve_ele->distance_valves[pv_to_v_map[i]] + 1, false);
                }

                pressure_valves[i].open = false;
                if(pressure > largest_pressure)
                    largest_pressure = pressure;
            }
        }

        if(hit) {
            if(me) {
                largest_pressure += valve_me->flow_rate * (time_limit - min_me);
            } else {
                largest_pressure += valve_ele->flow_rate * (time_limit - min_ele);
            }
            pressure_map[idx] = largest_pressure;
        }        

    } else {
        largest_pressure = pressure_map[idx];
    }

    return largest_pressure;
}

int main() {
    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        int i = 0;
        for(; buff[i] != ' '; ++i) { }
        char label[2];
        memcpy(label, &buff[++i], 2);
        int idx = find_valve(label);
        memcpy(valves[idx].label, label, 2);

        for(; buff[i] != '='; ++i) { }
        valves[idx].flow_rate = atoi(&buff[++i]);

        for(int n = 0; n < 5; ++n) {
            for(; buff[++i] != ' ';) { }
        }
        
        while(buff[i] != '\n' && buff[i] != '\0') {
            memcpy(label, &buff[++i], 2);
            int move_idx = find_valve(label);
            memcpy(valves[move_idx].label, label, 2);
            valves[idx].moves[valves[idx].moves_count++] = &valves[move_idx];
            i+=2;
            if(buff[i] == ',')
                ++i;
        }
    }

    //populate all valve distances to each other
    for(int i = 0; i < valves_count; ++i) {
        //reset valve distance for dijkstra
        for(int j = 0; j < valves_count; ++j)
            valves[j].distance = INT_MAX;
        
        //set start valve distance to 0
        valves[i].distance = 0;
        shortest_path_to_valve(&valves[i]);
        for(int j = 0; j < valves_count; ++j)
            valves[i].distance_valves[j] = valves[j].distance;
    }

    //open all non-pressure valves and store pressure valves in the
    //pressure valve array
    for(int i = 0; i < valves_count; ++i) {
        if(valves[i].flow_rate == 0)
            valves[i].open = true;
        else {
            pv_to_v_map[pressure_valves_count] = i;
            pressure_valves[pressure_valves_count++] = valves[i];
        }
    }

    //setup pressure map
    pressure_map = malloc(sizeof(int) * 0x2000000);
    for(int i = 0; i < 0x2000000; ++i)
        pressure_map[i] = -1;

    printf("%d\n", best_pressure(&valves[find_valve("AA")], 0));

    for(int i = 0; i < 0x2000000; ++i)
        pressure_map[i] = -1;

    time_limit = 26;

    //print_valve_distances();

    //printf("%d\n", best_pressure_2(&valves[find_valve("AA")], 0, 0));
    //answer is 2117, but currently broken...
    printf("%d\n", best_pressure_2(&valves[find_valve("AA")], &valves[find_valve("AA")], 0, 0, true));

    free(pressure_map);

    return 0;
}