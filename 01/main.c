#include <stdio.h>
#include <stdlib.h>
 
#define lbuff 4000
#define ldata lbuff

unsigned char data[ldata];


struct elf_inv {
    size_t items;
    int cal[100];
};

size_t inv_count = 0;
struct elf_inv inventories[1000] = {0};

int largest_cal(int largest_count) {
    int *largest_sum = (int *)malloc(largest_count * sizeof(int));
    for(int i = 0; i < largest_count; ++i)
        largest_sum[i] = 0;

    for(int i = 0; i < inv_count; ++i) {
        int cal_sum = 0;
        for(int j = 0; j < inventories[i].items; ++j) {
            cal_sum += inventories[i].cal[j];
        }

        if(cal_sum > largest_sum[largest_count - 1]) {
            largest_sum[largest_count - 1] = cal_sum;
            for(
                int j = largest_count - 1;
                j > 0 && largest_sum[j] > largest_sum[j - 1];
                --j
            ) {
                int tmp = largest_sum[j - 1];
                largest_sum[j - 1] = largest_sum[j];
                largest_sum[j] = tmp;
            }
        }
    }
    
    int total = 0;
    for(int i = 0; i < largest_count; ++i)
        total += largest_sum[i];

    free(largest_sum);

    return total;
}

int main()
{
    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        if(buff[0] == '\n' || buff[0] == '\0') {
            ++inv_count;
        } else
            inventories[inv_count].cal[inventories[inv_count].items++] = atoi(buff);
    }
    ++inv_count;
    
    printf("%d\n", largest_cal(1));
    
    printf("%d\n", largest_cal(3));

    return 0;
}