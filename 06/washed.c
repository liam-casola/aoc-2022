#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
 
#define lbuff 5000
#define ldata lbuff

int unique(char *data, int n) {
    int unique_count = 0;
    for(int i = 0; data[i] != '\0'; ++i) {
        bool test = false;
        for(int j = 1; j < n - unique_count && data[i + j] != '\0'; ++j) {
            if(data[i] == data[i + j]) {
                test = true;
                break;
            }
        }

        if(test) {
            unique_count = 0;
        } else {
            ++unique_count;
        }

        if(unique_count == n)
            return i + 1;
    }
}

int main()
{
    char buff[lbuff];
    fgets(buff, lbuff, stdin);

    printf("%d\n", unique(buff, 4));

    printf("%d\n", unique(buff, 14));

    return 0;
}