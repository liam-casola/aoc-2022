#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
 
#define lbuff 5000
#define ldata lbuff

int main()
{
    char buff[lbuff];
    fgets(buff, lbuff, stdin);

    int unique_4_count = 0;
    int unique_14_count = 0;
    for(int i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
        bool test_4 = false;
        for(int j = 1; j < 4 - unique_4_count && buff[i + j] != '\0'; ++j) {
            if(buff[i] == buff[i + j]) {
                test_4 = true;
            }
        }
        bool test_14 = false;
        for(int j = 1; j < 14 - unique_14_count && buff[i + j] != '\0'; ++j) {
            if(buff[i] == buff[i + j]) {
                test_14 = true;
            }
        }

        if(test_4)
            unique_4_count = 0;
        else
            unique_4_count++;

        if(test_14)
            unique_14_count = 0;
        else
            unique_14_count++;


        if(unique_4_count == 4)
            printf("%d \n", i + 1);

        if(unique_14_count == 14)
            printf("%d \n", i + 1);
    }

    return 0;
}