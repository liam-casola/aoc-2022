#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define lbuff 4000

struct grid {
    int x;
    int y;
    int width;
    int height;
    char *data;
};

char get(struct grid *grid, int x, int y) {
    return grid->data[(y - grid->y) * grid->width + x - grid->x];
}

void set(struct grid *grid, int x, int y, char c) {
    grid->data[(y - grid->y) * grid->width + x - grid->x] = c;
}

char * access(struct grid *grid, int x, int y) {
    return &grid->data[(y - grid->y) * grid->width + x - grid->x];
}

struct grid * create_grid(int x, int y, int width, int height) {
    struct grid *grid = malloc(sizeof(struct grid));
    grid->x = x;
    grid->y = y;
    grid->width = width;
    grid->height = height;
    grid->data = malloc(width * height);
    memset(grid->data, '.', width * height);

    return grid;
}

void delete_grid(struct grid *grid) {
    free(grid->data);
    free(grid);
}

struct grid * update_grid(struct grid *grid, int x, int y, int width, int height) {
    struct grid *dst = create_grid(x, y, width, height);

    for(int y = grid->y; y < grid->y + grid->height; ++y) {
        for(int x = grid->x; x < grid->x + grid->width; ++x) {
            set(dst, x, y, get(grid, x, y));
        }
    }

    delete_grid(grid);

    return dst;
}

void print_grid(struct grid *grid) {
    for(int y = grid->y; y < grid->y + grid->height; ++y) {
        for(int x = grid->x; x < grid->x + grid->width; ++x) {
            printf("%c", get(grid, x, y));
        }
        printf("\n");
    }
}

bool is_clear(struct grid *grid, int x, int y) {
    for(int xn = -1; xn <= 1; ++xn)
        for(int yn = -1; yn <= 1; ++yn) {
            if((xn != 0 || yn != 0) && get(grid, x + xn, y + yn) == '#')
                return false;
        }

    return true;
}

bool propose_dir(struct grid *grid, struct grid *try_move, int x, int y, int x_mov, int y_mov) {
    for(int i = -1; i <= 1; ++i)
        if(get(grid, x + i * y_mov + x_mov, y + i * x_mov + y_mov) == '#')
            return false;

    ++(*access(try_move, x + x_mov, y + y_mov));
    return true;
}

int move_dir(struct grid *grid, struct grid *prev_grid, struct grid *try_move, int x, int y, int x_mov, int y_mov) {
    for(int i = -1; i <= 1; ++i)
        if(get(prev_grid, x + i * y_mov + x_mov, y + i * x_mov + y_mov) == '#')
            return 0;

    if(get(try_move, x + x_mov, y + y_mov) != 1)
        return 1;

    set(grid, x + x_mov, y + y_mov, '#');
    set(grid, x, y, '.');
    return 2;
}

struct direction {
    int x_mov;
    int y_mov;
};

int dirs_pos = 0;
struct direction dirs[] = {{0, -1}, {0, 1}, {-1, 0}, {1, 0}};

bool simulate_round(struct grid *grid) {
    struct grid *prev_grid = create_grid(grid->x, grid->y, grid->width, grid->height);
    memcpy(prev_grid->data, grid->data, grid->width * grid->height);

    struct grid *try_move = create_grid(grid->x, grid->y, grid->width, grid->height);
    memset(try_move->data, 0, try_move->width * try_move->height);

    //propose moves for all elves
    for(int y = grid->y; y < grid->y + grid->height; ++y) {
        for(int x = grid->x; x < grid->x + grid->width; ++x) {
            if(get(grid, x, y) == '#') {
                if(is_clear(grid, x, y)) {
                    continue;
                }

                bool proposed = false;
                for(int i = 0; !proposed && i < 4; ++i) {
                    int index = (dirs_pos + i) % 4;
                    proposed = propose_dir(grid, try_move, x, y, dirs[index].x_mov, dirs[index].y_mov);
                }
            }
        }
    }

    unsigned any_move = 0;
    //moves all elves
    for(int y = grid->y; y < grid->y + grid->height; ++y) {
        for(int x = grid->x; x < grid->x + grid->width; ++x) {
            if(get(prev_grid, x, y) == '#') {
                if(is_clear(prev_grid, x, y)) {
                    continue;
                }

                int moved = 0;
                for(int i = 0; !moved && i < 4; ++i) {
                    int index = (dirs_pos + i) % 4;
                    moved = move_dir(grid, prev_grid, try_move, x, y, dirs[index].x_mov, dirs[index].y_mov);
                }
                any_move |= moved;
            }
        }
    }

    dirs_pos = (dirs_pos + 1) % 4;

    delete_grid(prev_grid);
    delete_grid(try_move);

    return any_move & 2;
}

int dot_area(struct grid *grid) {
    int x0 = INT_MAX;
    int x1 = INT_MIN;
    int y0 = INT_MAX;
    int y1 = INT_MIN;
    int elves = 0;
    for(int y = grid->y; y < grid->y + grid->height; ++y) {
        for(int x = grid->x; x < grid->x + grid->width; ++x) {
            if(get(grid, x, y) == '#') {
                if(x < x0)
                    x0 = x;
                if(x > x1)
                    x1 = x;
                if(y < y0)
                    y0 = y;
                if(y > y1)
                    y1 = y;
                ++elves;
            }
        }
    }

    return (x1 - x0 + 1) * (y1 - y0 + 1) - elves;
}

int main() {
    char *buff = malloc(lbuff);
    fgets(buff, lbuff, stdin);
    int side_length = strlen(buff) - 1;
    struct grid *grid = create_grid(0, 0, side_length, side_length);

    int counter = 0;
    do {
        memcpy(&grid->data[counter++ * side_length], buff, side_length);
    } while (fgets(buff, lbuff, stdin) != NULL);
    free(buff);

    grid = update_grid(grid, grid->x - 50, grid->y - 50, grid->width + 200, grid->height + 200);

    int round = 1;
    for(; round <= 10; ++round) {
        int res = simulate_round(grid);
    }

    printf("%d\n", dot_area(grid));

    while(simulate_round(grid)) {
        ++round;
    }

    printf("%d\n", round);

    delete_grid(grid);

    return 0;
}