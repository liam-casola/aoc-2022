#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define lbuff 4000
#define file_buff 6000

const int decryption_key = 811589153;

size_t file_count = 0;
long long int **file = NULL;
long long int **mix = NULL;

int idx_of_ptr(long long int *ptr) {
    for(int i = 0; i < file_count; ++i) {
        if(mix[i] == ptr)
            return i;
    }
}

void mix_file() {
    for(int i = 0; i < file_count; ++i) {
        int index = idx_of_ptr(file[i]);

        long long int divisor = (int)file_count - 1;
        int right_shift = ((*mix[index] % divisor) + divisor) % divisor;

        for(long long int j = 0; j < right_shift; ++j) {
            int next_index = (index + 1) % (int)file_count;
            long long int *tmp = mix[index];
            mix[index] = mix[next_index];
            mix[next_index] = tmp;
            index = next_index;
        }
    }
}

long long int grove() {
    int i = 0;
    for(; i < file_count; ++i) {
        if(*file[i] == 0)
            break;
    }
    int index = idx_of_ptr(file[i]);

    long long int M = *mix[(index + 1000) % (int)file_count];
    long long int MM = *mix[(index + 2000) % (int)file_count];
    long long int MMM = *mix[(index + 3000) % (int)file_count];

    return M + MM + MMM;
}

int main() {
    file = malloc(sizeof(long long int *) * file_buff);

    char *buff = malloc(lbuff);
    while (fgets(buff, lbuff, stdin) != NULL) {
        file[file_count] = malloc(sizeof(long long int));
        *file[file_count] = atoi(buff);
        ++file_count;
    }
    free(buff);

    mix = malloc(sizeof(long long int *) * file_count);
    memcpy(mix, file, sizeof(long long int *) * file_count);

    mix_file();

    printf("%d\n", grove());

    memcpy(mix, file, sizeof(long long int *) * file_count);

    for(int i = 0; i < file_count; ++i)
        *mix[i] *= decryption_key;

    for(int i = 0; i < 10; ++i) {
        mix_file();
    }

    printf("%lld\n", grove());

    free(mix);
    for(int i = 0; i < file_count; ++i)
        free(file[i]);
    free(file);

    return 0;
}