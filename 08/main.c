#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define lbuff 4000
#define ldata lbuff

int data[1000][1000];
int width = 0;
int height = 0;

int check_tree(int y, int x) {
    int visible_dir = 0;
    bool not_visible = false;
    //up
    for(int i = y - 1; i >= 0; --i)
        not_visible |= !(data[i][x] < data[y][x]);
    if(y == 0)
        visible_dir++;
    else
        visible_dir += !not_visible;
    not_visible = false;
    //down
    for(int i = y + 1; i < height; ++i)
        not_visible |= !(data[i][x] < data[y][x]);
    if(y == height - 1)
        visible_dir++;
    else
        visible_dir += !not_visible;
    not_visible = false;
    //left
    for(int i = x - 1; i >= 0; --i)
        not_visible |= !(data[y][i] < data[y][x]);
    if(x == 0)
        visible_dir++;
    else
        visible_dir += !not_visible;
    not_visible = false;
    //right
    for(int i = x + 1; i < width; ++i)
        not_visible |= !(data[y][i] < data[y][x]);
    if(x == width - 1)
        visible_dir++;
    else
        visible_dir += !not_visible;
    not_visible = false;

    //printf("(%d,%d):%d ", x,y,visible_dir);

    return visible_dir;
}

int check_visible() {
    int count = 0;

    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            if(check_tree(i, j))
                count++;
        }
    }

    return count;
}

int check_tree_2(int y, int x) {
    int score = 1;
    int trees = 0;
    //up
    for(int i = y - 1; i >= 0; --i)
        if(data[i][x] < data[y][x])
            trees++;
        else {
            trees++;
            break;
        }
    score *= trees;
    trees = 0;

    //down
    for(int i = y + 1; i < height; ++i)
        if(data[i][x] < data[y][x])
            trees++;
        else {
            trees++;
            break;
        }
    score *= trees;
    trees = 0;
    //left
    for(int i = x - 1; i >= 0; --i)
        if(data[y][i] < data[y][x])
            trees++;
        else {
            trees++;
            break;
        }
    score *= trees;
    trees = 0;
    //right
    for(int i = x + 1; i < width; ++i)
        if(data[y][i] < data[y][x])
            trees++;
        else {
            trees++;
            break;
        }
    score *= trees;

    return score;
}

int check_blocked() {
    int score = 0;

    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            int res = check_tree_2(i, j);
            if(res > score)
                score = res;
        }
    }

    return score;
}

void print_forest() {
    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++)
            printf("%d", data[i][j]);
        printf("\n");
    }
}

int main()
{
    char buff[lbuff];
    int tmp = 0;
    while (fgets(buff, lbuff, stdin) != NULL) {
        for(int i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
            data[height][i] = buff[i] - '0';
            if(!tmp)
                width++;
        }
        tmp = 1;
        height++;
    }

    //print_forest();

    printf("%d\n", check_visible());

    printf("%d\n", check_blocked());

    return 0;
}