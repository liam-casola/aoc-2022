#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define lbuff 11000

char *buff = NULL;
int length = 0;

char pieces[5][16] = {
    {'.','.','.','.',
     '.','.','.','.',
     '.','.','.','.',
     '#','#','#','#',},
    {'.','.','.','.',
     '.','#','.','.',
     '#','#','#','.',
     '.','#','.','.',},
    {'.','.','.','.',
     '.','.','#','.',
     '.','.','#','.',
     '#','#','#','.',},
    {'#','.','.','.',
     '#','.','.','.',
     '#','.','.','.',
     '#','.','.','.',},
    {'.','.','.','.',
     '.','.','.','.',
     '#','#','.','.',
     '#','#','.','.',},
};

struct piece {
    int x; //lower left
    int y; //lower left
    char *data;
};

struct chamber {
    int width;
    int height;
    int highest_y;
    char *grid;
};

int next_push() {
    static int pos = 0;
    if(pos == length)
        pos = 0;
    return buff[pos++] - '=';
}

char * next_piece() {
    static int pos = 0;
    if(pos == sizeof(pieces) / sizeof(pieces[0]))
        pos = 0;
    return pieces[pos++];
}

char * tile(struct chamber *chamber, int x, int y) {
    return &chamber->grid[x + chamber->width * (chamber->height - 1 - y)];
}

char rock(struct piece *rock, int x, int y) {
    return rock->data[x + 4 * (4 - 1 - y)];
}

//-1 stopped, 1 moved
int move(struct chamber *chamber, struct piece *piece, int dx, int dy) {
    bool blocked = false;
    //check push is possible
    for(int y = 0; y < 4; ++y) {
        for(int x = 0; x < 4; ++x) {
            char r = rock(piece, x, y);
            if(
                r == '#' && 
                (piece->x + x + dx < 0 || piece->x + x + dx >= chamber->width || *tile(chamber, piece->x + x + dx, piece->y + y) == '#')
            ) {
                //if push would be blocked
                blocked = true;
            }
        }
    }

    //push the rock
    if(!blocked)
        piece->x += dx;

    //check drop is possible
    for(int y = 0; y < 4; ++y) {
        for(int x = 0; x < 4; ++x) {
            char r = rock(piece, x, y);
            if(
                r == '#' &&
                (piece->y + y + dy < 0 || *tile(chamber, piece->x + x, piece->y + y + dy) == '#')
            ) {
                //if drop would be stopped, return stopped
                return -1;
            }
        }
    }

    piece->y += dy;

    return 1;
}

void drop_into_chamber(struct chamber *chamber) {
    struct piece piece;
    piece.x = 2;
    piece.y = chamber->highest_y + 3;
    piece.data = next_piece();
    //printf("%.*s\n", 16, piece.data);
    //drop piece
    int rock_state = 1;
    while(rock_state != -1) {
        int push = next_push();
        rock_state = move(chamber, &piece, push, -1);
    }

    //copy piece in
    for(int y = 0; y < 4; ++y)
        for(int x = 0; x < 4; ++x)
            if(rock(&piece, x, y) == '#')
                *tile(chamber, piece.x + x, piece.y + y) = rock(&piece, x, y);

    //update highest
    for(int y = chamber->highest_y; y < chamber->height; ++y) {
        bool has_tiles = false;
        for(int x = 0; x < chamber->width; ++x) {
            if(*tile(chamber, x, y) == '#') {
                has_tiles = true;
            }
        }

        if(!has_tiles) {
            chamber->highest_y = y;
            break;
        }
    }
}

int find_pattern(struct chamber *chamber) {
    //amount to start by such that tiles can no longer be affected by yet to fall pieces
    int fall_offset = 1000;

    char *start = tile(chamber, 0, chamber->highest_y - 1 - fall_offset);
    int rows = 1;
    char *next = tile(chamber, 0, chamber->highest_y - rows - 1 - fall_offset);
    char *next_2 = tile(chamber, 0, chamber->highest_y - rows * 2 - 1 - fall_offset);
    char *next_3 = tile(chamber, 0, chamber->highest_y - rows * 3 - 1 - fall_offset);
    char *next_4 = tile(chamber, 0, chamber->highest_y - rows * 4 - 1 - fall_offset);

    while(memcmp(start, next, rows * chamber->width) || memcmp(start, next_2, rows * chamber->width) ||
            memcmp(start, next_3, rows * chamber->width) || memcmp(start, next_4, rows * chamber->width)) {
        ++rows;
        next = tile(chamber, 0, chamber->highest_y - rows - 1 - fall_offset);
        next_2 = tile(chamber, 0, chamber->highest_y - rows * 2 - 1 - fall_offset);
        next_3 = tile(chamber, 0, chamber->highest_y - rows * 3 - 1 - fall_offset);
        next_4 = tile(chamber, 0, chamber->highest_y - rows * 4 - 1 - fall_offset);
    }

    return rows;
}

int main() {
    buff = malloc(lbuff);
    fgets(buff, lbuff, stdin);
    length = strlen(buff) - 1;

    struct chamber chamber;
    chamber.width = 7,
    chamber.height = 100000,
    chamber.highest_y = 0,
    chamber.grid = malloc(chamber.width * chamber.height);
    for(int i = 0; i < chamber.width * chamber.height; ++i)
        chamber.grid[i] = '.';

    int pieces_1 = 2022;
    int pieces_2 = 10000;

    for(int i = 0; i < pieces_1; ++i)
        drop_into_chamber(&chamber);

    // for(int y = 22; y >= 0; --y)
    //     printf("|%.*s| %d\n", 7, tile(&chamber, 0, y), y + 1);
    // printf("+-------+\n");

    printf("%d\n", chamber.highest_y);

    //drop additional 10000 tiles so there are enough
    //to find a pattern
    for(int i = 0; i < pieces_2; ++i)
        drop_into_chamber(&chamber);

    long long int pattern_length = find_pattern(&chamber);

    int count = 0;
    int start = chamber.highest_y;
    while(chamber.highest_y - start != pattern_length) {
        drop_into_chamber(&chamber);
        ++count;
    }

    long long int pieces_so_far = pieces_1 + pieces_2 + count;

    long long int pieces_left_to_drop = (1000000000000 - pieces_so_far) % count;

    for(int i = 0; i < pieces_left_to_drop; ++i)
        drop_into_chamber(&chamber);

    pieces_so_far = pieces_1 + pieces_2 + count + pieces_left_to_drop;
    pieces_left_to_drop = (1000000000000 - pieces_so_far) % count;

    long long int patterns_count = (1000000000000 - pieces_so_far) / count;

    printf("%lld\n", patterns_count * pattern_length + chamber.highest_y);

    free(buff);
    free(chamber.grid);
    return 0;
}