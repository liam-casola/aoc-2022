#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define lbuff 4000

int time_limit = 24;

struct BOM {
    int ore;
    int clay;
    int obsidian;
};

struct blueprint {
    int id;
    struct BOM robots[4];
};

struct state {
    int ores[4];
    int robots[4];
};

size_t blueprints_count = 0;
struct blueprint *blueprints = NULL;

int simulate_blueprint(struct blueprint *blueprint, int pattern[time_limit], int *robots) {
    struct state simulation = { .robots[0] = 1 };

    int time = 0;
    while(time < time_limit) {
        //initiate builds if any
        bool building = false;
        if(
            simulation.ores[0] >= blueprint->robots[pattern[*robots]].ore &&
            simulation.ores[1] >= blueprint->robots[pattern[*robots]].clay &&
            simulation.ores[2] >= blueprint->robots[pattern[*robots]].obsidian
        ) {
            //printf("building: ");
            simulation.ores[0] -= blueprint->robots[pattern[*robots]].ore;
            simulation.ores[1] -= blueprint->robots[pattern[*robots]].clay;
            simulation.ores[2] -= blueprint->robots[pattern[*robots]].obsidian;
            building = true;
        }
        //printf("%d ", *robots);

        //initiate collection
        for(int i = 0; i < 4; ++i)
            simulation.ores[i] += simulation.robots[i];


        //complete builds if any
        if(building)
            ++simulation.robots[pattern[(*robots)++]];

        ++time;
    }

    return simulation.ores[3];
}

int blueprint_trials() {
    int sum = 0;
    int *robot_build_order = malloc(sizeof(int) * time_limit);

    for(int i = 0; i < blueprints_count; ++i) {
        int largest = 0;
        memset(robot_build_order, 0, sizeof(int) * time_limit);
        while(robot_build_order[0] != 2) {
            int robots = 0;
            int res = simulate_blueprint(&blueprints[i], robot_build_order, &robots);
            if(res > largest)
                largest = res;

            //reset robots after total built
            //memset(&robot_build_order[robots], 0, sizeof(int) * (time_limit - robots));
            for(int r = robots; r < time_limit; ++r)
                robot_build_order[r] = 0;

            int index = robots - 1;
            for(; index >= 0; --index) {
                robot_build_order[index] += 1;
                if(robot_build_order[index] > 3) {
                    robot_build_order[index] = 0;
                } else {
                    break;
                }
            }
        }

        //printf("%d -> %d\n", largest, largest * blueprints[i].id);
        sum += largest * blueprints[i].id;
    }

    free(robot_build_order);

    return sum;
}

//the guess at number of robots that will be built before all robots are geo robots
const int geo_offset = 22;
int blueprint_trials_eaten() {
    int product = 1;
    int *robot_build_order = malloc(sizeof(int) * time_limit);

    for(int i = 0; i < blueprints_count; ++i) {
        int largest = 0;
        memset(robot_build_order, 0, sizeof(int) * time_limit);
        for(int r = geo_offset; r < time_limit; ++r)
            robot_build_order[r] = 3;

        while(robot_build_order[0] != 2) {
            int robots = 0;
            int res = simulate_blueprint(&blueprints[i], robot_build_order, &robots);
            if(res > largest)
                largest = res;

            for(int r = robots; r < geo_offset; ++r)
                robot_build_order[r] = 0;

            int index = robots - 1;
            for(; index >= 0; --index) {
                robot_build_order[index] += 1;
                if(robot_build_order[index] > 3) {
                    robot_build_order[index] = 0;
                } else {
                    break;
                }
            }
            for(int r = geo_offset; r < time_limit; ++r)
                robot_build_order[r] = 3;
        }

        //printf("%d\n", largest);
        product *= largest;
    }

    free(robot_build_order);

    return product;
}

int main() {
    blueprints = malloc(sizeof(struct blueprint) * 50);

    char *buff = malloc(lbuff);
    while (fgets(buff, lbuff, stdin) != NULL) {
        //zero BOMs
        blueprints[blueprints_count] = (const struct blueprint){ 0 };

        int i = 0;
        while(buff[i] < '0' || buff[i] > '9') { ++i; } //bp #
        blueprints[blueprints_count].id = atoi(&buff[i]);
        while(buff[i] != ' ') { ++i; } //skip past bp #
        while(buff[i] < '0' || buff[i] > '9') { ++i; } // ore robot ore cost
        blueprints[blueprints_count].robots[0].ore = atoi(&buff[i]);
        while(buff[i] != ' ') { ++i; } //skip past ore #

        while(buff[i] < '0' || buff[i] > '9') { ++i; } // clay robot ore cost
        blueprints[blueprints_count].robots[1].ore = atoi(&buff[i]);
        while(buff[i] != ' ') { ++i; } //skip past ore #

        while(buff[i] < '0' || buff[i] > '9') { ++i; } // obsidian robot ore cost
        blueprints[blueprints_count].robots[2].ore = atoi(&buff[i]);
        while(buff[i] != ' ') { ++i; } //skip past ore #
        while(buff[i] < '0' || buff[i] > '9') { ++i; } // obsidian robot clay cost
        blueprints[blueprints_count].robots[2].clay = atoi(&buff[i]);
        while(buff[i] != ' ') { ++i; } //skip past clay #

        while(buff[i] < '0' || buff[i] > '9') { ++i; } // geode robot ore cost
        blueprints[blueprints_count].robots[3].ore = atoi(&buff[i]);
        while(buff[i] != ' ') { ++i; } //skip past ore #
        while(buff[i] < '0' || buff[i] > '9') { ++i; } // geode robot obsidian cost
        blueprints[blueprints_count].robots[3].obsidian = atoi(&buff[i]);

        ++blueprints_count;
    }
    free(buff);

    int silver = blueprint_trials();
    printf("%d\n", silver);

    time_limit = 32;
    if(blueprints_count > 3)
        blueprints_count = 3;

    int gold = blueprint_trials_eaten();
    printf("%d\n", gold);

    free(blueprints);

    return 0;
}