#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>
 
#define lbuff 4000

struct square {
    int elevation;
    int steps;
};

int width = 0;
int height = 0;
struct square squares[10000];
struct square *start;

size_t lowest_count = 0;
struct square *lowest[10000];

struct square * get(int x, int y) {
    return &squares[x + y * width];
}

void set_steps(int x, int y, int steps) {
    squares[x + y * width].steps = steps;
}

void solve_reverse(int x, int y) {
    int current_steps = get(x, y)->steps;

    //up
    if(
        y != 0 &&
        get(x, y - 1)->elevation + 1 >= get(x, y)->elevation && //if the square is reachable
        current_steps + 1 < get(x, y - 1)->steps //if there would be a step improvement
    ) {
        set_steps(x, y - 1, current_steps + 1);
        solve_reverse(x, y - 1);
    }
    //down
    if(
        y + 1 < height &&
        get(x, y + 1)->elevation + 1 >= get(x, y)->elevation &&
        current_steps + 1 < get(x, y + 1)->steps
    ) {
        set_steps(x, y + 1, current_steps + 1);
        solve_reverse(x, y + 1);
    }
    //left
    if(
        x != 0 &&
        get(x - 1, y)->elevation + 1 >= get(x, y)->elevation &&
        current_steps + 1 < get(x - 1, y)->steps
    ) {
        set_steps(x - 1, y, current_steps + 1);
        solve_reverse(x - 1, y);
    }
    //right
    if(
        x + 1 < width &&
        get(x + 1, y)->elevation + 1 >= get(x, y)->elevation &&
        current_steps + 1 < get(x + 1, y)->steps
    ) {
        set_steps(x + 1, y, current_steps + 1);
        solve_reverse(x + 1, y);
    }
}

int main()
{
    char buff[lbuff];
    int end_x;
    int end_y;
    bool tmp = false;
    while (fgets(buff, lbuff, stdin) != NULL) {
        for(int i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
            squares[height * width + i].steps = INT_MAX;
            if(buff[i] == 'S') {
                squares[height * width + i].elevation = 'a' - 'a';
                start = &squares[height * width + i];
            } else if(buff[i] == 'E') {
                squares[height * width + i].elevation = 'z' - 'a';
                end_x = i;
                end_y = height;
            } else {
                squares[height * width + i].elevation = buff[i] - 'a';
            }

            if(squares[height * width + i].elevation == 0) {
                lowest[lowest_count++] = &squares[height * width + i];
            }

            if(!tmp)
                width++;
        }
        tmp = true;
        height++;
    }

    set_steps(end_x, end_y, 0);
    solve_reverse(end_x, end_y);

    printf("%d\n", start->steps);

    int shortest = INT_MAX;
    for(int i = 0; i < lowest_count; ++i) {
        if(lowest[i]->steps < shortest)
            shortest = lowest[i]->steps;
    }

    printf("%d\n", shortest);

    return 0;
}