#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
 
#define lbuff 4000

struct list;

enum value_type {
    vt_list,
    vt_int
};

struct value {
    enum value_type type;
    union {
        struct list *list;
        int integer;
    };
};

struct list {
    //struct list *parent;
    size_t count;
    struct value *values;
};

char buff[lbuff];
char nextChar() {
    static int pos = 0;
    if(buff[pos] == '\n' || buff[pos] == '\0') {
        pos = 0;
        return 0;
    }
    return buff[pos++];
}

struct list * parse_list() {
    struct list *list = malloc(sizeof(struct list));
    list->count = 0;
    list->values = NULL;
    char c;
    for(c = nextChar(); c != ']';) {
        if(c != ',')
            list->values = realloc(list->values, sizeof(struct value) * ++list->count);

        if(c == '[') { //new list
            list->values[list->count - 1].type = vt_list;
            list->values[list->count - 1].list = parse_list();
        }
        
        if(c >= '0' && c <= '9') {
            int integer_count = 0;
            char integer[10];
            while(c >= '0' && c <= '9') { //integer value
                integer[integer_count++] = c;
                c = nextChar();
            }
            integer[integer_count] = '\0';
            list->values[list->count - 1].type = vt_int;
            list->values[list->count - 1].integer = atoi(integer);
        } else {
            c = nextChar();
        }
    }

    return list;
}

void print_list(struct list *list) {
    printf("[");
    for(int i = 0; i < list->count; ++i) {
        if(list->values[i].type == vt_list) {
            print_list(list->values[i].list);
        }

        if(list->values[i].type == vt_int) {
            printf("%d", list->values[i].integer);
        }

        if(i < list->count - 1)
            printf(",");
    }
    printf("]");
}

void print_packets(struct list **packets, size_t count) {
    for(int i = 0; i < count; ++i) {
        print_list(packets[i]);
        printf("\n");
    }
}

//------------part1------------
int compare_lists(struct list *left, struct list *right) {
    int right_order = 0;

    int i;
    for(i = 0; i < left->count && i < right->count && !right_order; ++i) {
        // If both values are integers
        if(left->values[i].type == vt_int && right->values[i].type == vt_int) {
            //printf("%d vs %d\n", left->values[i].integer, right->values[i].integer);
            // If the left integer is higher than the right integer, the inputs are not in the right order
            if(left->values[i].integer < right->values[i].integer)
                right_order = 1;
            if(left->values[i].integer > right->values[i].integer)
                right_order = -1;
        } else
        // If both values are lists
        if (left->values[i].type == vt_list && right->values[i].type == vt_list) {
            right_order = compare_lists(left->values[i].list, right->values[i].list);
        }
        // If exactly one value is an integer
        else {
            if(left->values[i].type == vt_int) {
                struct list new_left;
                new_left.count = 1;
                new_left.values = malloc(sizeof(struct value));
                new_left.values->type = vt_int;
                new_left.values->integer = left->values[i].integer;

                right_order = compare_lists(&new_left, right->values[i].list);
                free(new_left.values);
            }

            if(right->values[i].type == vt_int) {
                struct list new_right;
                new_right.count = 1;
                new_right.values = malloc(sizeof(struct value));
                new_right.values->type = vt_int;
                new_right.values->integer = right->values[i].integer;

                right_order = compare_lists(left->values[i].list, &new_right);
                free(new_right.values);
            }
        }
    }

    //If the left list runs out of items first, the inputs are in the right order
    if(i == left->count && i != right->count && !right_order)
        right_order = 1;

    // If the right list runs out of items first, the inputs are not in the right order
    if(i == right->count && i != left->count && !right_order)
        right_order = -1;

    return right_order;
}

int right_order(struct list **packets, size_t count) {
    int sum = 0;

    for(int i = 0; i < count; i+=2) {
        if(compare_lists(packets[i], packets[i+1]) == 1) {
            sum += i / 2 + 1;
        }
    }

    return sum;
}

//------------part2------------
int decoder_key(struct list **packets, size_t count) {
    packets = realloc(packets, sizeof(struct list *) * (count + 2));

    //divider packet [[2]]
    packets[count] = malloc(sizeof(struct list));
    struct list *div2 = packets[count];
    packets[count]->count = 1;
    packets[count]->values = malloc(sizeof(struct value));
    packets[count]->values->type = vt_int;
    packets[count]->values->integer = 2;
    count++;

    //divider packet [[6]]
    packets[count] = malloc(sizeof(struct list));
    struct list *div6 = packets[count];
    packets[count]->count = 1;
    packets[count]->values = malloc(sizeof(struct value));
    packets[count]->values->type = vt_int;
    packets[count]->values->integer = 6;
    count++;

    for(int i = 0; i < count; ++i) {
        for(int j = 0; j < count - 1; ++j) {
            if(compare_lists(packets[j], packets[j+1]) == -1) {
                struct list *tmp = packets[j];
                packets[j] = packets[j+1];
                packets[j+1] = tmp;
            }
        }
    }

    int key = 1;

    for(int i = 0; i < count; ++i) {
        if(packets[i] == div2)
            key *= (i + 1);
        
        if(packets[i] == div6)
            key *= (i + 1);
    }

    return key;
}

int main()
{
    size_t packets_count = 0;
    struct list **packets = NULL;
    while (fgets(buff, lbuff, stdin) != NULL) {
        if(buff[0] != '\n') {
            packets = realloc(packets, sizeof(struct list *) * ++packets_count);
            nextChar();
            packets[packets_count - 1] = parse_list();
            nextChar();
        }
    }

    //print_packets(packets, packets_count);

    printf("%d\n", right_order(packets, packets_count));

    printf("%d\n", decoder_key(packets, packets_count));

    return 0;
}