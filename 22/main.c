#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define lbuff 8000

enum ACTION {
    MOV,
    ROT
};

struct inst {
    enum ACTION action;
    int value;
};

struct pos {
    int x;
    int y;
    int dir;
};

struct edge_jump {
    int x;
    int y;
    int dir;
    int rot;
    struct edge_jump *jmp;
};

const int side_length = 50;
int width = 150;
int height = 0;
char *board = NULL;
char *board_draw = NULL;

size_t insts_count = 0;
size_t insts_size = 6000;
struct inst *insts = NULL;

void traverse_horizontal(struct pos *pos, int x) {
    int mov = pos->dir == 0 ? 1 : -1;
    int next_x = pos->x;
    for(int i = 0; i < x; ++i) {
        next_x += mov;
        next_x = (next_x % width + width) % width;
        //skip empty tiles
        if(board[pos->y * width + next_x] == ' ') {
            //wrap and find the next valid tile
            while(board[pos->y * width + next_x] == ' ') {
                next_x += mov;
                next_x = (next_x % width + width) % width;
            }
        }
        //decide what to do based on the next x
        if(board[pos->y * width + next_x] == '#') {
            break;
        } else {
            //mov > 0 ? printf("right ") : printf("left ");
            pos->x = next_x;
        }
    }
}

void traverse_vertical(struct pos *pos, int y) {
    int mov = pos->dir == 1 ? 1 : -1;
    int next_y = pos->y;
    for(int i = 0; i < y; ++i) {
        next_y += mov;
        next_y = (next_y % height + height) % height;
        //skip empty tiles
        if(board[next_y * width + pos->x] == ' ') {
            //wrap and find the next valid tile
            while(board[next_y * width + pos->x] == ' ') {
                next_y += mov;
                next_y = (next_y % height + height) % height;
            }
        }
        //decide what to do based on the next x
        if(board[next_y * width + pos->x] == '#') {
            break;
        } else {
            //mov > 0 ? printf("down ") : printf("up ");
            pos->y = next_y;
        }
    }
}

void do_inst(struct pos *pos, struct inst *inst) {
    switch (inst->action) {
    case MOV:
        //moving left or right
        if(pos->dir % 2 == 0)
            traverse_horizontal(pos, inst->value);
        else
            traverse_vertical(pos, inst->value);
        break;
    case ROT:
        pos->dir = ((pos->dir + inst->value) % 4 + 4) % 4;
        break;
    }

    //printf("\nx:%d y:%d d:%d\n", pos->x, pos->y, pos->dir);
}

int walk_path() {
    struct pos pos;
    pos.y = 0;
    pos.dir = 0;
    for(int i = 0; i < width; ++i) {
        if(board[i] == '.') {
            pos.x = i;
            break;
        }
    }
    //printf("x:%d y:%d d:%d\n", pos.x, pos.y, pos.dir);
    for(int i = 0; i < insts_count; ++i) {
        do_inst(&pos, &insts[i]);
    }

    return 1000 * (pos.y + 1) + 4 * (pos.x + 1) + pos.dir;
}

//-----------part2-----------
size_t edge_count = 0;
struct edge_jump edges[14*100];

void add_edge(int x0, int y0, int dir0, int mov0, int x1, int y1, int dir1, int mov1, int rot) {
    //edge forward
    for(int i = 0; i < side_length; ++i) {
        unsigned forward_index = edge_count++;
        unsigned reverse_index = edge_count++;
        edges[forward_index].dir = dir0;
        edges[forward_index].rot = rot;
        edges[forward_index].jmp = &edges[reverse_index];
        edges[reverse_index].dir = dir1;
        edges[reverse_index].rot = rot * -1;
        edges[reverse_index].jmp = &edges[forward_index];
        if(dir0 % 2 != 0) {//horizontal edge
            edges[forward_index].x = x0 + i * mov0;
            edges[forward_index].y = y0;
        } else {
            edges[forward_index].x = x0;
            edges[forward_index].y = y0 + i * mov0;
        }

        if(dir1 % 2 != 0) {//horizontal edge
            edges[reverse_index].x = x1 + i * mov1;
            edges[reverse_index].y = y1;
        } else {
            edges[reverse_index].x = x1;
            edges[reverse_index].y = y1 + i * mov1;
        }
    }
}

void map_edges() {
    //*****************
    //* example input *
    //*****************
    // //edge (8,0):1 entered from ^, matchng edge (3,4):-1 entered from ^ : -2 rotation
    // add_edge(8, 0, 3, 1, 3, 4, 3, -1, -2);
    // //edge (8,0):1 entered from <, matchng edge (4,4):1 entered from ^ : -1 rotation
    // add_edge(8, 0, 2, 1, 4, 4, 3, 1, -1);
    // //edge (11,0):1 entered from >, matchng edge (15,11):-1 entered from > : 2 rotation
    // add_edge(11, 0, 0, 1, 15, 11, 0, -1, 2);
    // //edge (11,4):1 entered from >, matchng edge (15,8):-1 entered from ^ : 1 rotation
    // add_edge(11, 4, 0, 1, 15, 8, 3, -1, 1);
    // //edge (0,4):1 entered from <, matchng edge (15,11):-1 entered from v : 1 rotation
    // add_edge(0, 4, 2, 1, 15, 11, 1, -1, 1);
    // //edge (0,7):1 entered from v, matchng edge (11,11):-1 entered from v : -2 rotation
    // add_edge(0, 7, 1, 1, 11, 11, 1, -1, -2);
    // //edge (4,7):1 entered from v, matchng edge (8,11):-1 entered from < : -1 rotation
    // add_edge(4, 7, 1, 1, 8, 11, 2, -1, -1);

    //edge (50,0):1 entered from ^, matching edge (0,150):1 entered from < : 1 rotation
    add_edge(50, 0, 3, 1, 0, 150, 2, 1, 1);
    //edge (100,0):1 entered from ^, matchng edge (0,199):1 entered from v : 0 rotation
    add_edge(100, 0, 3, 1, 0, 199, 1, 1, 0);
    //edge (149,0):1 entered from >, matchng edge (99,149):-1 entered from > : 2 rotation
    add_edge(149, 0, 0, 1, 99, 149, 0, -1, 2);
    //edge (100,49):1 entered from v, matchng edge (99,50):1 entered from > : 1 rotation
    add_edge(100, 49, 1, 1, 99, 50, 0, 1, 1);
    //edge (50,149):1 entered from v, matchng edge (49,150):1 entered from > : 1 rotation
    add_edge(50, 149, 1, 1, 49, 150, 0, 1, 1);
    //edge (50,0):1 entered from <, matchng edge (0,149):-1 entered from < : 2 rotation
    add_edge(50, 0, 2, 1, 0, 149, 2, -1, 2);

    //edge (50,50):1 entered from <, matchng edge (0,100):1 entered from ^ : -1 rotation
    add_edge(50, 50, 2, 1, 0, 100, 3, 1, -1);
}

int read_edge(int *x, int *y, int dir) {
    int i = 0;
    for(; i < edge_count; ++i)
        if(edges[i].x == *x && edges[i].y == *y && edges[i].dir == dir)
            break;
    
    *x = edges[i].jmp->x;
    *y = edges[i].jmp->y;
    return edges[i].rot;
}

int traverse_cube_horizontal(struct pos *pos, int x) {
    int mov = pos->dir == 0 ? 1 : -1;
    int next_x = pos->x;
    int next_y = pos->y;
    for(int i = 0; i < x; ++i) {
        next_x += mov;
        
        //if going off face
        if(next_x < 0 || next_x == width || board[next_y * width + next_x] == ' ') {
            next_x -= mov; //reset back to the edge
            int rot = read_edge(&next_x, &next_y, pos->dir);
            if(board[next_y * width + next_x] == '#') {
                break;
            } else {
                pos->x = next_x;
                pos->y = next_y;
                pos->dir = ((pos->dir + rot) % 4 + 4) % 4;
                return x - i - 1;
            }
            
        }

        //decide what to do based on the next x
        if(board[next_y * width + next_x] == '#') {
            break;
        } else {
            //mov > 0 ? printf("right ") : printf("left ");
            pos->x = next_x;
            pos->y - next_y;
        }
    }

    return 0;
}

int traverse_cube_vertical(struct pos *pos, int y) {
    int mov = pos->dir == 1 ? 1 : -1;
    int next_x = pos->x;
    int next_y = pos->y;
    for(int i = 0; i < y; ++i) {
        next_y += mov;
        
        //if going off face
        if(next_y < 0 || next_y == height || board[next_y * width + next_x] == ' ') {
            next_y -= mov; //reset back to the edge
            int rot = read_edge(&next_x, &next_y, pos->dir);
            if(board[next_y * width + next_x] == '#') {
                break;
            } else {
                pos->x = next_x;
                pos->y = next_y;
                pos->dir = ((pos->dir + rot) % 4 + 4) % 4;
                return y - i - 1;
            }
        }

        //decide what to do based on the next x
        if(board[next_y * width + pos->x] == '#') {
            break;
        } else {
            //mov > 0 ? printf("down ") : printf("up ");
            pos->y = next_y;
        }
    }

    return 0;
}

void do_cube_inst(struct pos *pos, struct inst *inst) {
    switch (inst->action) {
    case MOV: ;
        int amount_to_move = inst->value;
        while(amount_to_move > 0) {
            if(pos->dir % 2 == 0)
                amount_to_move = traverse_cube_horizontal(pos, amount_to_move);
            else
                amount_to_move = traverse_cube_vertical(pos, amount_to_move);
        }
        break;
    case ROT:
        pos->dir = ((pos->dir + inst->value) % 4 + 4) % 4;
        break;
    }
}

int walk_cube() {
    struct pos pos;
    pos.y = 0;
    pos.dir = 0;
    for(int i = 0; i < width; ++i) {
        if(board[i] == '.') {
            pos.x = i;
            break;
        }
    }

    map_edges();

    for(int i = 0; i < insts_count; ++i) {
        do_cube_inst(&pos, &insts[i]);
    }

    return 1000 * (pos.y + 1) + 4 * (pos.x + 1) + pos.dir;
}

int main() {
    insts = malloc(sizeof(struct inst) * insts_size);

    char *buff = malloc(lbuff);
    fgets(buff, lbuff, stdin);
    while (*buff != '\n') {
        ++height;
        board = realloc(board, width * height);

        bool EOL = false;
        for(int i = 0; i < width; ++i) {
            if(buff[i] != '\n' && !EOL)
                board[(height - 1) * width + i] = buff[i];
            else {
                EOL = true;
                board[(height - 1) * width + i] = ' ';
            }
        }
        fgets(buff, lbuff, stdin);
    }
    fgets(buff, lbuff, stdin);
    int counter = 0;
    while(buff[counter] != '\0' && buff[counter] != '\n') {
        insts[insts_count].action = MOV;
        insts[insts_count].value = atoi(&buff[counter]);
        ++insts_count;
        while(buff[counter] >= '0' && buff[counter] <= '9') { ++counter; }
        if(buff[counter] == 'R') {
            insts[insts_count].action = ROT;
            insts[insts_count].value = 1;
            ++insts_count;
            ++counter;
        }
        if(buff[counter] == 'L') {
            insts[insts_count].action = ROT;
            insts[insts_count].value = -1;
            ++insts_count;
            ++counter;
        }
    }
    free(buff);

    board_draw = malloc(width * height);
    memcpy(board_draw, board, width * height);

    printf("%d\n", walk_path());

    map_edges();

    // for(int i = 0; i < edge_count; ++i)
    //     printf("x:%d, y:%d, entered from:%d, rotates:%d\n", edges[i].x, edges[i].y, edges[i].dir, edges[i].rot);

    printf("%d\n", walk_cube());

    return 0;
}