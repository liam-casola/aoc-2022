#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
 
#define lbuff 4000
#define ldata lbuff

struct motion {
    char dir;
    int steps;
};

size_t moitions_count = 0;
struct motion motions[2500];

struct pos {
    int x;
    int y;
};

struct pos rope[10] = {0};

size_t visited_count = 1;
struct pos visited[10000];

void add_visited(int x, int y) {
    for(int i = 0; i < visited_count; ++i)
        if(visited[i].x == x && visited[i].y == y)
            return;
    
    visited[visited_count].x = x;
    visited[visited_count++].y = y;
}

void move_tail(struct pos *tail, struct pos *head, bool flag) {
    if(tail->x == head->x && tail->y == head->y)
        return;

    int hor_d = head->x - tail->x;
    int ver_d = head->y - tail->y;

    int hor = 0;
    int ver = 0;

    bool diag = abs(hor_d) + abs(ver_d) > 2;

    if(hor_d > 1 || (hor_d > 0 && diag)) hor = 1;
    if(hor_d < -1 || (hor_d < 0 && diag)) hor = -1;
    if(ver_d > 1 || (ver_d > 0 && diag)) ver = 1;
    if(ver_d < -1 || (ver_d < 0 && diag)) ver = -1;

    tail->x += hor;
    tail->y += ver;

    if(flag)
        add_visited(tail->x, tail->y);
}

void move_head(struct pos *head, struct motion *motion, int knots) {
    int hor = 0;
    int ver = 0;

    switch (motion->dir)
    {
    case 'U':
        ver = 1;
        break;
    case 'D':
        ver = -1;
        break;
    case 'R':
        hor = 1;
        break;
    case 'L':
        hor = -1;
        break;
    }

    for(int i = 0; i < motion->steps; ++i) {
        head->x += hor;
        head->y += ver;
        for(int j = 1; j < knots - 1; ++j)
            move_tail(&rope[j], &rope[j-1], false);
        move_tail(&rope[knots - 1], &rope[knots - 2], true);
    }
}

int simulate(int knots) {
    visited_count = 1;
    for(int i = 0; i < 10; ++i) {
        rope[i].x = 0;
        rope[i].y = 0;
    }

    for(int i = 0; i < moitions_count; ++i)
        move_head(&rope[0], &motions[i], knots);

    return visited_count;
}

int main()
{
    char buff[lbuff];
    int tmp = 0;
    while (fgets(buff, lbuff, stdin) != NULL) {
        motions[moitions_count].dir = buff[0];
        motions[moitions_count++].steps = atoi(&buff[2]);
    }

    visited[0].x = 0;
    visited[0].y = 0;

    printf("%d\n", simulate(2));

    printf("%d\n", simulate(10));

    return 0;
}