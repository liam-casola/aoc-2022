#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#define lbuff 4000
#define ldata lbuff

unsigned char data[ldata];

struct stack {
    size_t count;
    char crates[100];
};

int highest_stack = 1;
struct stack stacks[20] = {0};
struct stack stacks_backup[20] = {0};

struct step {
    int quantity;
    int start;
    int end;
};

size_t steps_count = 0;
struct step steps[1000];

void move_crate(struct stack *input, struct stack *output) {
    output->crates[output->count] = input->crates[input->count - 1];
    ++output->count;
    --input->count;
}

void move_crates(struct step *step) {
    for(int i = 0; i < step->quantity; ++i)
        move_crate(&stacks[step->start], &stacks[step->end]);
}

void rearrangement() {
    for(int i = 0; i < steps_count; ++i)
        move_crates(&steps[i]);
}

void move_crates_9001(struct step *step) {
    stacks[step->start].count -= step->quantity;
    for(int i = 0; i < step->quantity; ++i) {
        stacks[step->end].crates[stacks[step->end].count++] = stacks[step->start].crates[stacks[step->start].count + i];
    }
}

void rearrangement_9001() {
    for(int i = 0; i < steps_count; ++i)
        move_crates_9001(&steps[i]);
}

void insert_bottom_stack(struct stack *stack, char c) {
    for(int i = stack->count; i > 0; --i) {
        stack->crates[i] = stack->crates[i - 1];
    }

    stack->crates[0] = c;
    ++stack->count;
}

void print_stacks() {
    for(int i = 1; i <= highest_stack; ++i) {
        printf("%d: ", i);
        for(int j = 0; j < stacks[i].count; ++j)
            printf("[%c] ", stacks[i].crates[j]);
        printf("\n");
    }
}

void crate_message(char *msg) {
    for(int i = 1; i <= highest_stack; ++i) {
        msg[i - 1] = stacks[i].crates[stacks[i].count - 1];
    }
    msg[highest_stack] = '\0';
}

int main()
{
    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL && buff[1] != '1') {
        //insert crates onto stacks
        for(int i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
            if((i - 1) % 4 == 0 && buff[i] != ' ') {
                insert_bottom_stack(&stacks[(i - 1) / 4 + 1], buff[i]);
                if((i - 1) / 4 > highest_stack)
                    highest_stack = (i - 1) / 4 + 1;
            }
        }
    }
    fgets(buff, lbuff, stdin); //consume empty line
    while (fgets(buff, lbuff, stdin) != NULL) {
        //obtain steps
        int i = 5;
        steps[steps_count].quantity = atoi(&buff[i]);
        for(; buff[i] != 'm'; ++i) { }
        steps[steps_count].start = atoi(&buff[i + 2]);
        for(; buff[i] != 'o'; ++i) { }
        steps[steps_count].end = atoi(&buff[i + 2]);

        ++steps_count;
    }

    memcpy(stacks_backup, stacks, sizeof(stacks));
    rearrangement();

    //print_stacks();

    char *msg = (char *)malloc(highest_stack + 1);
    crate_message(msg);

    printf("%s\n", msg);

    memcpy(stacks, stacks_backup, sizeof(stacks));
    rearrangement_9001();
    crate_message(msg);

    printf("%s\n", msg);

    free(msg);

    return 0;
}