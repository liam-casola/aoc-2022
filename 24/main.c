#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define lbuff 4000

struct map {
    int width;
    int height;
    unsigned char *data;
};

enum blizzard {
    EMPTY,
    RIGHT = 0x1,
    DOWN  = 0x2,
    LEFT  = 0x4,
    UP    = 0x8,
    WALL  = 0x10
};

unsigned char * access(struct map *map, int x, int y) {
    return &map->data[y * map->width + x];
}

void print_map(struct map *map) {
    for(int y = 0; y < map->height; ++y) {
        for(int x = 0; x < map->width; ++x) {
            char display;
            switch(*access(map, x, y)) {
                case WALL: display = '#'; break;
                case RIGHT: display = '>'; break;
                case DOWN: display = 'v'; break;
                case LEFT: display = '<'; break;
                case UP: display = '^'; break;
                case EMPTY: display = '.';
            }

            int count = 0;
            for(int i = 0; i < 4; ++i) {
                count += (*access(map, x, y) >> i) & 1;
            }
            if(count > 1)
                display = '0' + count;

            printf("%c", display);
        }
        printf("\n");
    }
}

void step_map(struct map *map) {
    struct map tmp;
    tmp.width = map->width;
    tmp.height = map->height;
    tmp.data = malloc(tmp.width * tmp.height);
    memcpy(tmp.data, map->data, tmp.width * tmp.height);

    //empty blizzards from map
    for(int y = 0; y < map->height; ++y) {
        for(int x = 0; x < map->width; ++x) {
            if(!(*access(&tmp, x, y) & WALL)) {
                *access(map, x, y) = EMPTY;
            }
        }
    }

    //place update blizzards
    for(int y = 0; y < map->height; ++y) {
        for(int x = 0; x < map->width; ++x) {
            if(*access(&tmp, x, y) & RIGHT) {
                if(*access(&tmp, x + 1, y) & WALL) {
                    *access(map, 1, y) |= RIGHT;
                } else {
                    *access(map, x + 1, y) |= RIGHT;
                }
            }

            if(*access(&tmp, x, y) & LEFT) {
                if(*access(&tmp, x - 1, y) & WALL) {
                    *access(map, map->width - 2, y) |= LEFT;
                } else {
                    *access(map, x - 1, y) |= LEFT;
                }
            }

            if(*access(&tmp, x, y) & DOWN) {
                if(*access(&tmp, x, y + 1) & WALL) {
                    *access(map, x, 2) |= DOWN;
                } else {
                    *access(map, x, y + 1) |= DOWN;
                }
            }

            if(*access(&tmp, x, y) & UP) {
                if(*access(&tmp, x, y - 1) & WALL) {
                    *access(map, x, map->height - 3) |= UP;
                } else {
                    *access(map, x, y - 1) |= UP;
                }
            }
        }
    }

    free(tmp.data);
}

struct pos {
    int x;
    int y;
};

int lists_counts[2] = {0}; //number of elements in list 1 & 2
struct pos lists[2][10000]; //list of positions to swap between

void add_to_list(int flip, int x, int y) {
    for(int i = 0; i < lists_counts[!flip]; ++i)
        if(lists[!flip][i].x == x && lists[!flip][i].y == y)
            return; //already exists, do nothing
    
    lists[!flip][lists_counts[!flip]].x = x;
    lists[!flip][lists_counts[!flip]].y = y;
    ++lists_counts[!flip];
}

struct result {
    int silver;
    int gold;
};

struct result path_blizzard(struct map *map, struct pos goals[2]) {
    int flip = 0;
    bool goals_flip = 0;
    lists[0][0] = (struct pos){goals[1].x, goals[1].y};
    lists_counts[0] = 1;
    lists_counts[1] = 0;

    struct result result;

    int found = 0;
    int steps = 0;
    while(found < 3) {
        //update map so that new valid positions are visible
        step_map(map);

        //try and move all in the current coordinate list
        for(int i = 0; i < lists_counts[flip]; ++i) {
            int x = lists[flip][i].x;
            int y = lists[flip][i].y;

            if(x == goals[goals_flip].x && y == goals[goals_flip].y) {
                //dump the lists
                lists_counts[flip] = 0;
                lists_counts[!flip] = 0;
                //switch start and final pos
                goals_flip = !goals_flip;
                //current can continue adding to the now empty lists to go back
                ++found;
                if(found == 1)
                    result.silver = steps;
                if(found == 3)
                    result.gold = steps;
            }

            //if staying still is possible
            if(!*access(map, x, y)) {
                add_to_list(flip, x, y);
            }

            //if going right is possible
            if(!*access(map, x + 1, y)) {
                add_to_list(flip, x + 1, y);
            }

            //if going left is possible
            if(!*access(map, x - 1, y)) {
                add_to_list(flip, x - 1, y);
            }

            //if going down is possible
            if(!*access(map, x, y + 1)) {
                add_to_list(flip, x, y + 1);
            }

            //if going up is possible
            if(!*access(map, x, y - 1)) {
                add_to_list(flip, x, y - 1);
            }
        }

        lists_counts[flip] = 0; //clear buffer
        flip = !flip;
        ++steps;
    }

    return result;
}

int main() {
    struct map map;
    map.height = 1; //extra row to block start position
    map.data = NULL;

    char *buff = malloc(lbuff);
    while(fgets(buff, lbuff, stdin) != NULL) {
        map.width = strlen(buff) - 1;
        ++map.height;
        map.data = realloc(map.data, map.width * map.height);

        for(int i = 0; i < map.width; ++i) {
            switch (buff[i]) {
            case '#': map.data[(map.height - 1) * map.width + i] = WALL; break;
            case '>': map.data[(map.height - 1) * map.width + i] = RIGHT; break;
            case 'v': map.data[(map.height - 1) * map.width + i] = DOWN; break;
            case '<': map.data[(map.height - 1) * map.width + i] = LEFT; break;
            case '^': map.data[(map.height - 1) * map.width + i] = UP; break;
            default: map.data[(map.height - 1) * map.width + i] = EMPTY;
            }
        }
    }
    free(buff);

    //extra row to block end position
    ++map.height;
    map.data = realloc(map.data, map.width * map.height);
    //fill in extra rows with walls so start & end node can't go off the map
    for(int x = 0; x < map.width; ++x) {
        *access(&map, x, 0) = WALL;
        *access(&map, x, map.height - 1) = WALL;
    }

    //populate final and start goals
    struct pos goals[2];
    goals[0].y = map.height - 2;
    goals[1].y = 1;
    for(int x = 0; x < map.width; ++x) {
        if(*access(&map, x, goals[0].y) == EMPTY)
            goals[0].x = x;

        if(*access(&map, x, goals[1].y) == EMPTY)
            goals[1].x = x;
    }

    struct result result = path_blizzard(&map, goals);

    printf("%d\n", result.silver);

    printf("%d\n", result.gold);

    free(map.data);

    return 0;
}