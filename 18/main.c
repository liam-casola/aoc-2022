#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define lbuff 4000

#define max_cubes 10000

struct pos {
    int x;
    int y;
    int z;
};

size_t cubes_count = 0;
struct pos *cubes = NULL;
struct pos *cubes_backup = NULL;
struct pos largest = { INT_MIN, INT_MIN, INT_MIN };
struct pos smallest = { INT_MAX, INT_MAX, INT_MAX };

int visible_faces(struct pos *cube) {
    int exposed = 6;

    for(int i = 0; i < cubes_count; ++i) {
        int distance = abs(cubes[i].x - cube->x) + abs(cubes[i].y - cube->y) + abs(cubes[i].z - cube->z);
        if(distance == 1)
            --exposed;
    }

    return exposed;
}

int surface_area() {
    int sa = 0;

    for(int i = 0; i < cubes_count; ++i)
        sa += visible_faces(&cubes[i]);

    return sa;
}

int fill(struct pos *cube) {
    if(cube->x > largest.x || cube->y > largest.y || cube->z > largest.z
    || cube->x < smallest.x || cube->y < smallest.y || cube->z < smallest.z)
        return -1;

    for(int i = 0; i < cubes_count; ++i) {
        if(cubes[i].x == cube->x && cubes[i].y == cube->y && cubes[i].z == cube->z) {
            return 0;
        }
    }

    cubes[cubes_count].x = cube->x;
    cubes[cubes_count].y = cube->y;
    cubes[cubes_count].z = cube->z;
    ++cubes_count;

    int filled = 1;

    for(int x = -1; x < 2; x+=2) {
        struct pos next_cube = { cube->x + x, cube->y, cube->z };
        int res = fill(&next_cube);
        if(res == -1)
            return -1;
        filled += res;
    }

    for(int y = -1; y < 2; y+=2) {
        struct pos next_cube = { cube->x, cube->y + y, cube->z };
        int res = fill(&next_cube);
        if(res == -1)
            return -1;
        filled += res;
    }

    for(int z = -1; z < 2; z+=2) {
        struct pos next_cube = { cube->x, cube->y, cube->z + z };
        int res = fill(&next_cube);
        if(res == -1)
            return -1;
        filled += res;
    }

    return filled;
}

void fill_voids() {
    for(int x = smallest.x; x < largest.x + 1; ++x)
        for(int y = smallest.y; y < largest.y + 1; ++y)
            for(int z = smallest.z; z < largest.z + 1; ++z) {
                memcpy(cubes_backup, cubes, sizeof(struct pos) * max_cubes);
                int cubes_count_backup = cubes_count;

                struct pos filler = {x, y, z};
                int res = fill(&filler);
                if(res < 0) {
                    memcpy(cubes, cubes_backup, sizeof(struct pos) * max_cubes);
                    cubes_count = cubes_count_backup;
                }
            }
}

int main() {
    cubes = malloc(sizeof(struct pos) * max_cubes);
    cubes_backup = malloc(sizeof(struct pos) * max_cubes);

    char *buff = malloc(lbuff);
    while (fgets(buff, lbuff, stdin) != NULL) {
        int i = 0;
        cubes[cubes_count].x = atoi(&buff[i]);
        while(buff[i] != ',') { ++i; }
        cubes[cubes_count].y = atoi(&buff[++i]);
        while(buff[i] != ',') { ++i; }
        cubes[cubes_count].z = atoi(&buff[++i]);

        if(cubes[cubes_count].x > largest.x) largest.x = cubes[cubes_count].x;
        if(cubes[cubes_count].x < smallest.x) smallest.x = cubes[cubes_count].x;
        if(cubes[cubes_count].y > largest.y) largest.y = cubes[cubes_count].y;
        if(cubes[cubes_count].y < smallest.y) smallest.y = cubes[cubes_count].y;
        if(cubes[cubes_count].z > largest.z) largest.z = cubes[cubes_count].z;
        if(cubes[cubes_count].z < smallest.z) smallest.z = cubes[cubes_count].z;

        ++cubes_count;
    }
    free(buff);

    printf("%d\n", surface_area());

    fill_voids();

    printf("%d\n", surface_area());

    free(cubes_backup);
    free(cubes);
    return 0;
}