#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define lbuff 4000

long long int llipow(long long int x, int exp) {
    long long int ret = 1;
    for(int i = 0; i < exp; ++i)
        ret *= x;

    return ret;
}

int elven_numerals_count = 0;
long long int elven_numerals[150];

int main() {
    char *buff = malloc(lbuff);
    while(fgets(buff, lbuff, stdin) != NULL) {
        int length = strlen(buff) - 1;
        elven_numerals[elven_numerals_count] = 0;
        for(int i = 0; buff[i] != '\n' && buff[i] != '\0'; ++i) {
            switch (buff[i]) {
            case '=': elven_numerals[elven_numerals_count] += llipow(5, length - 1 - i) * -2; break;
            case '-': elven_numerals[elven_numerals_count] += llipow(5, length - 1 - i) * -1; break;
            case '0': elven_numerals[elven_numerals_count] += llipow(5, length - 1 - i) * 0; break;
            case '1': elven_numerals[elven_numerals_count] += llipow(5, length - 1 - i) * 1; break;
            case '2': elven_numerals[elven_numerals_count] += llipow(5, length - 1 - i) * 2; break;
            }
        }
        //printf("%lld\n", elven_numerals[elven_numerals_count]);
        ++elven_numerals_count;
    }
    free(buff);

    long long int sum = 0;
    for(int i = 0; i < elven_numerals_count; ++i)
        sum += elven_numerals[i];

    //printf("%lld\n", sum);

    int snafu_count = 0;
    char snafu[50];
    while(sum != 0) {
        int res = sum % 5;
        switch (res) {
        case 0: snafu[snafu_count] = '0'; break;
        case 1: snafu[snafu_count] = '1'; break;
        case 2: snafu[snafu_count] = '2'; break;
        case 3: snafu[snafu_count] = '='; sum += 2; break;
        case 4: snafu[snafu_count] = '-'; sum += 1; break;
        }
        sum /= 5;

        ++snafu_count;
    }

    for(int i = snafu_count - 1; i >= 0; --i)
        printf("%c", snafu[i]);
    printf("\n");

    return 0;
}