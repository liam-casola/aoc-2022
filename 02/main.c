#include <stdio.h>
#include <stdlib.h>
 
#define lbuff 4000
#define ldata lbuff

unsigned char data[ldata];

enum SCORE {
    LOSE = 0,
    DRAW = 3,
    WIN = 6
};

enum RPS {
    ROCK = 1,
    PAPER = 2,
    SCISSORS = 3
};

struct round {
    enum RPS abc;
    enum RPS xyz;
    int score;
    enum SCORE outcome;
};

size_t round_count = 0;
struct round rounds[10000];

enum RPS decode_ABC(char c) {
    switch (c)
    {
    case 'A': return ROCK;
    case 'B': return PAPER;
    case 'C': return SCISSORS;
    }
    printf("error ABC\n");
}

enum RPS decode_XYZ(char c) {
    switch (c)
    {
    case 'X': return ROCK;
    case 'Y': return PAPER;
    case 'Z': return SCISSORS;
    }
    printf("error XYZ\n");
}

enum SCORE decode_outcome(char c) {
    switch (c)
    {
    case 'X': return LOSE;
    case 'Y': return DRAW;
    case 'Z': return WIN;
    }
    printf("error outcome\n");
}

void score_round(struct round *round) {
    if(round->abc == round->xyz)
        round->score = round->xyz + DRAW;
    else if(round->abc % 3 == round->xyz - 1)
        round->score = round->xyz + WIN;
    else
        round->score = round->xyz + LOSE;
}

int total_score() {
    int total = 0;
    for(int i = 0; i < round_count; ++i)
        total += rounds[i].score;

    return total;
}

void rescore(struct round *round) {
    if(round->outcome == DRAW)
        round->score = round->abc + DRAW;
    else if(round->outcome == WIN)
        round->score = round->abc % 3 + 1 + WIN;
    else
        round->score = (round->abc + 1) % 3 + 1 + LOSE;
}

int main()
{
    char buff[lbuff];
    while (fgets(buff, lbuff, stdin) != NULL) {
        rounds[round_count].abc = decode_ABC(buff[0]);
        rounds[round_count].xyz = decode_XYZ(buff[2]);
        rounds[round_count].outcome = decode_outcome(buff[2]);
        score_round(&rounds[round_count]);
        ++round_count;
    }

    printf("%d\n", total_score());

    for(int i = 0; i < round_count; ++i)
        rescore(&rounds[i]);

    printf("%d\n", total_score());

    return 0;
}